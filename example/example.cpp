
#include <inGui.h>
#include <iostream>
#include <thread>

using namespace std;

int main (int argc, char* argv[]) {
  // Render window creation
  sf::RenderWindow app(sf::VideoMode(800, 600, 32), "inGui", sf::Style::Close);
  app.UseVerticalSync(true);

  // Gui initialization
  inGui::Gui gui(800, 600);
  auto& lua = gui.getLuaState();

  // Define an empty function "update".
  // This function can be implemented by the application to receive updates
  // after each iteration of the main loop.
  lua.runString("function update(time)\n end\n");

  // Load the application
  if (argc > 1) {
    lua.runFile(argv[1]);
  } else {
    lua.runFile("widgets.lua");
  }

  // Main loop
  sf::Clock clock;
  while (app.IsOpened()) {
    // Event handling
    sf::Event event;
    while (app.GetEvent(event)) {
      if (event.Type == sf::Event::Closed ||
	  (event.Type == sf::Event::KeyPressed &&
	   event.Key.Code == sf::Key::Escape)) {
	//Window closed => exit program
	app.Close();
	break;
      }

      // Transmit event to the gui
      gui.PushInput(event);
    }
    
    // Logical treatment (event handling, ...)
    lua.getGlobals()["update"](clock.GetElapsedTime());
    clock.Reset();
    gui.Logic();
    
    // Clear screen (fill with black)
    app.Clear();
    
    // We draw the gui
    gui.Draw(app);
    
    // We display to the screen
    app.Display();
    
    // Sleep for a while
    this_thread::sleep_for(chrono::milliseconds(20));
  }

  return EXIT_SUCCESS;
}
