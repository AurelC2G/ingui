
inGui.Fonts:registerFont("arial", "arial.ttf")
inGui.Gui:setDefaultFont("arial", 30)

local Game = {}
local config = {
  blockSize = 25;
  gameSize = {10, 22};
  points = {1, 3, 6, 10};
}
local frame
local nextFrame
local scoreLabel

-- Current state of the board: a grid with individual widgets or nil
grid = {}
for x = 1,config.gameSize[1] do
  grid[x] = {}
  for y = 1,config.gameSize[2] do
    grid[x][y] = nil
  end
end

-- Pieces definition
local piecesTypes = {
  {
    -- Cube
    {0,0}, {0,-1}, {1,0}, {1,-1};
    rotation = false;
    color = {0, 200, 50};
  };
  {
    -- Bar
    {-1,0}, {0,0}, {1,0}, {2,0};
    rotation = true;
    color = {0, 0, 255};
  };
  {
    -- L
    {0,-1}, {0,0}, {0,1}, {1,1};
    rotation = true;
    color = {255, 0, 0};
  };
  {
    -- Inverted L
    {0,-1}, {0,0}, {0,1}, {-1,1};
    rotation = true;
    color = {0, 255, 255};
  };
  {
    -- S
    {0,0}, {1,0}, {0,1}, {-1,1};
    rotation = true;
    color = {0, 0, 0};
  };
  {
    -- Inverted S
    {0,0}, {-1,0}, {0,1}, {1,1};
    rotation = true;
    color = {255, 128, 0};
  };
  {
    -- T
    {0,0}, {-1,0}, {1,0}, {0,-1};
    rotation = true;
    color = {255, 255, 0};
  };
}

function paintBlockAt(block, x, y)
  block:setPosition(
    (x + block.x - 1) * config.blockSize,
    (y + block.y - 1) * config.blockSize)
end

class 'Piece'

function Piece:__init()
  local def = piecesTypes[math.random(7)]
  self.canRotate = def.rotation

  self.blocks = {}
  for i = 1,4 do
    -- XXX Icon is probably better but doesn't support border
    self.blocks[i] = inGui.createWidget("Frame", {
      size = {config.blockSize, config.blockSize};
      border = {1, inGui.Color(0, 0, 0, 255)};
      backgroundColor = def.color;
    })
    self.blocks[i].x = def[i][1]
    self.blocks[i].y = def[i][2]
    self.blocks[i].paintAt = paintBlockAt
  end

  -- Initial rotation at random
  if self.canRotate then
    for i = 2,math.random(4) do
      self:doRotate(1)
    end
  end
end

function Piece:doRotate(dir)
  for b in values(self.blocks) do
    b.x, b.y = - dir * b.y, dir * b.x
  end
end

function Piece:rotate()
  if self.canRotate then
    self:doRotate(1)
    if self:isColliding() then
      self:doRotate(-1)
    end
  end
end

function Piece:move(dx)
  self.x = self.x + dx
  if self:isColliding() then
    self.x = self.x - dx
  else
    self:draw()
  end
end

function Piece:down()
  self.y = self.y + 1
  if self:isColliding() then
    self.y = self.y - 1
    self:drop()
  end
end

function Piece:draw()
  for b in values(self.blocks) do
    b:paintAt(self.x, self.y)
  end
end

function Piece:isColliding()
  for b in values(self.blocks) do
    local x = self.x + b.x
    local y = self.y + b.y
    if (x < 1 or
        x > config.gameSize[1] or
        y < 1 or
        y > config.gameSize[2] or
        (grid[x][y] ~= nil)) then
      return true
    end
  end
  return false
end

function Piece:introduce()
  self.x = math.ceil(config.gameSize[1] / 2)
  self.y = 1

  -- Push the piece down to fit in the frame
  for b in values(self.blocks) do
    if self.y + b.y < 1 then
      self.y = 1 - b.y
    end
  end

  if self:isColliding() then
    return false
  end

  for b in values(self.blocks) do
    frame:add(b)
  end
  self:draw()

  return true
end

function Piece:drop()
  for b in values(self.blocks) do
    grid[self.x + b.x][self.y + b.y] = b
    b.x = 0; b.y = 0
  end

  self.blocks = {}
  Game.onPieceDropped()
end



function Game.createNewPiece()
  Game.piece = Game.nextPiece
  for b in values(Game.nextPiece.blocks) do
    b:detach()
  end

  if not Game.piece:introduce() then
    print("Game over!")
    Game.running = false
    return
  end

  Game.nextPiece = Piece()
  for b in values(Game.nextPiece.blocks) do
    nextFrame:add(b)
    b:paintAt(3, 3)
  end
end

function Game.start()
  Game.score = 0
  Game.running = true
  Game.elapsed = 0
  Game.dropping = false
  Game.nextPiece = Piece()
  Game.createNewPiece()
end

function Game.keyPressed(keyCode)
  if not Game.running then
    return false
  end

  local p = Game.piece
  if keyCode == inGui.KeyCodes.Left then
    p:move(-1)
  elseif keyCode == inGui.KeyCodes.Right then
    p:move(1)
  elseif keyCode == inGui.KeyCodes.Up then
    p:rotate()
  elseif keyCode == inGui.KeyCodes.Down then
    if not Game.dropping and Game.elapsed > 0.04 then
      Game.elapsed = 0.04
    end
    Game.dropping = true
  else
    return false
  end
  p:draw()
  return false
end

function Game.keyReleased(keyCode)
  if keyCode == inGui.KeyCodes.Down then
    Game.dropping = false
    return true
  end
  return false
end

function update(time)
  if not Game.running then
    return
  end

  Game.elapsed = Game.elapsed + time

  local costForMove = 1.0
  if Game.dropping then
    costForMove = 0.04
  end

  while Game.elapsed >= costForMove do
    Game.elapsed = Game.elapsed - costForMove
    local p = Game.piece
    p:down()
  end
  Game.piece:draw()
end

function Game.onPieceDropped()
  local y = config.gameSize[2]
  local linesScored = 0
  local firstScored
  while y > 0 and (linesScored == 0 or y >= firstScored - 3) do
    local isFull = true
    for x = 1,config.gameSize[1] do
      if grid[x][y] == nil then
        isFull = false
        break
      end
    end

    if isFull then
      linesScored = linesScored + 1
      if linesScored == 1 then
        firstScored = y
      end
      for x = 1,config.gameSize[1] do
        grid[x][y]:detach()
      end
    end
    y = y - 1
  end

  if linesScored > 0 then
    for y = firstScored,linesScored+1,-1 do
      for x = 1,config.gameSize[1] do
        grid[x][y] = grid[x][y-linesScored]
        if grid[x][y] ~= nil then
          grid[x][y]:paintAt(x, y)
        end
      end
    end
    for y = 1,linesScored do
      for x = 1,config.gameSize[1] do
        grid[x][y] = nil
      end
    end

    Game.score = Game.score + config.points[linesScored]
    scoreLabel:onScoreUpdate(Game.score)
  end

  Game.createNewPiece()
end

-- Draw the game box
frame = inGui.createWidget("Frame", {
  size = {
    config.gameSize[1] * config.blockSize;
    config.gameSize[2] * config.blockSize;
  };
  border = {3, inGui.Color(255, 0, 0, 255)};
  backgroundColor = {105, 235, 190};
  focusable = true;
})
frame:attachToRoot()
frame:requestModalFocus()
function frame:onKeyPressed(key)
  return Game.keyPressed(key.code)
end
function frame:onKeyReleased(key)
  return Game.keyReleased(key.code)
end

-- Draw vertical lines
for i = 1,(config.gameSize[1]-1) do
  frame:addNew("Frame", {
    position = {i * config.blockSize, 0};
    size = {1, config.gameSize[2] * config.blockSize};
    backgroundColor = {120, 120, 120};
  })
end

-- Draw the box for the next piece
nextFrame = inGui.createWidget("Frame", {
  size = {
    6 * config.blockSize;
    6 * config.blockSize;
  };
  border = {3, inGui.Color(255, 0, 0, 255)};
  backgroundColor = {105, 235, 190};
  position = { frame:getWidth() + config.blockSize; config.blockSize }
})
nextFrame:attachToRoot()

-- Draw the score box
scoreLabel = inGui.createWidget("Label", {
  position = {350, 200};
  color = {255, 0, 0};
  text = "0";
})
function scoreLabel:onScoreUpdate(newScore)
  self:setText(tostring(newScore))
end
scoreLabel:attachToRoot()

Game.start()
