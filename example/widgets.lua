
inGui.Fonts:registerFont("arial", "arial.ttf")
inGui.Gui:setDefaultFont("arial", 20)

function createLayout()
  local layout = inGui.createWidget("Table", {
    numberOfColumns = 3;
    horizontalSpacing = 15;
    verticalSpacing = 30;
    position = {15, 15};
  })
  layout:attachToRoot()

  layout:add(createRegularFrame())
  layout:add(createFocusableFrame())

  local button = layout:addNew("Button", {
    text = "I am a button";
  })
  button:adjustSize()

  layout:add(createColorFrame())
  layout:add(createTextField())
  layout:add(createHoverableFrame())

  for i = 1,21 do
    layout:addNew("Label", {
      text = "Placeholder";
      color = {255, 255, 255};
    })
  end
end

function createRegularFrame()
  local frame = inGui.createWidget("Frame", {
    size = {250, 100};
    border = {2, inGui.Color(255, 0, 0, 255)};
  })

  frame:addNew("Label", {
    text = "This is a frame";
    color = {0, 0, 0};
    position = {5, 5};
  })

  frame:addNew("Label", {
    text = "You can put stuff inside";
    color = {0, 0, 0};
    font = {"arial", 14};
    position = {40, 65};
  })

  return frame
end

function createFocusableFrame()
  local frame = inGui.createWidget("Frame", {
    focusable = true;
    size = {250, 100};
    border = {3, inGui.Color(150, 150, 250, 255)};
  })

  frame:addNew("Label", {
    text = "This is a focusable frame";
    position = {5, 5};
  })

  local label = frame:addNew("Label", {
    font = {"arial", 14};
    position = {40, 65};
  })

  function frame:onFocusGained ()
     self:setBackgroundColor(255, 0, 0)
    label:setText("Nice! I am focused now.")
    return true
  end
  function frame:onFocusLost ()
    self:setBackgroundColor(0, 0, 255)
    label:setText("(click me!)")
    return true
  end
  frame:onFocusLost()

  return frame
end

function createColorFrame()
  local frame = inGui.createWidget("Frame", {
    focusable = true;
    size = {250, 100};
    border = {3, inGui.Color(150, 150, 250, 255)};
  })

  frame:addNew("Label", {
    text = "Use your mouse wheel!";
    color = {0, 0, 0};
    font = {"arial", 18};
    position = {5, 5};
  })

  local layout = frame:addNew("Table", {
    numberOfColumns = 3;
    horizontalSpacing = 30;
    verticalSpacing = 5;
    position = {45, 35};
    font = {"arial", 14};
  })

  layout:addNew("Label", {
    text = "Red";
    color = {0, 0, 0};
  })

  layout:addNew("Label", {
    text = "Green";
    color = {0, 0, 0};
  })

  layout:addNew("Label", {
    text = "Blue";
    color = {0, 0, 0};
  })

  local fields = {}
  for i = 1,3 do
    fields[i] = layout:addNew("Label", {
      text = "255";
      color = {0, 0, 0};
    })
  end

  local cb = {}
  for i = 1,3 do
    cb[i] = layout:addNew("Checkbox", {
      checked = true;
    })
  end

  local colors = {'r'; 'g'; 'b'}
  function frame:onMouseWheel(delta, x, y)
    local color = self:getBackgroundColor()

    for i = 1,3 do
      local c = colors[i]
      if cb[i]:isChecked() then
        color[c] = color[c] + delta * 10
        if color[c] > 255 then
          color[c] = 0
        end
        fields[i]:setText(tostring(color[c]))
      end
    end

    self:setBackgroundColor(color)

    return true
  end

  return frame
end


function createTextField()
  local field = inGui.createWidget("TextField", {
    size = {200, 50};
    text = "This is a text field";
  })

  function field:onSubmit()
    self:setText("")
  end

  return field
end

function createHoverableFrame()
  local frame = inGui.createWidget("Frame", {
    size = {250, 100};
    focusable = true;
    border = {3, inGui.Color(150, 150, 250, 255)};
  })

  local label = frame:addNew("Label", {
    position = {5, 5};
  })

  function frame:onMouseOver()
    self:setBackgroundColor(255, 0, 0)
    label:setText("Nice! I am hovered now.")
    return true
  end
  function frame:onMouseOut()
    self:setBackgroundColor(0, 0, 255)
    label:setText("This is a hoverable frame")
    return true
  end
  frame:onMouseOut()

  return frame
end


createLayout()
