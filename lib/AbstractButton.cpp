/**
 * inGui
 *
 * Author : aurelien.fredouelle@gmail.com
 */

#include "AbstractButton.h"

using namespace std;

namespace inGui {

AbstractButton::AbstractButton(Gui* gui)
    : Widget(gui)
    , mPressed(false)
    , mEnabled(true) {
  setFocusable(true);
}

bool AbstractButton::isEnabled() const {
  return mEnabled;
}

void AbstractButton::setEnabled(bool e) {
  mEnabled = e;
  if (!mEnabled) {
    mPressed = false;
  }

  setFocusable(e);
}

bool AbstractButton::onKeyPressed(sf::Event::KeyEvent key) {
  if (mEnabled && key.Code == sf::Key::Return) {
    mPressed = true;
    return true;
  }

  return false;
}

bool AbstractButton::onKeyReleased(sf::Event::KeyEvent key) {
  if (key.Code == sf::Key::Return && mPressed) {
    mPressed = false;
    if (mEnabled) {
      luabind::call_function<void>(mLuaObject["onSubmit"], mLuaObject);
      return true;
    }
  }
  
  return false;
}

bool AbstractButton::onMouseButtonPressed(int button, float x, float y) {
  if (mEnabled && button == sf::Mouse::Left) {
    mPressed = true;
  }
  
  return true;
}

bool AbstractButton::onMouseButtonReleased(int button, float x, float y) {
  if (mPressed && button == sf::Mouse::Left) {
    mPressed = false;
    if (mEnabled) {
      try {
	luabind::object callback = mLuaObject["onSubmit"];
	luabind::call_function<void>(callback, mLuaObject);
      } catch (luabind::error e) {
	cout << "Lua error : " << lua_tostring(e.state(), -1) << endl;
      }
    }
  }

  return true;
}

} // namespace
