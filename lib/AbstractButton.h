/**
 * inGui
 *
 * Author : aurelien.fredouelle@gmail.com
 */

#pragma once

#include "Widget.h"

namespace inGui {

class INGUI_API AbstractButton : public Widget {
 public:
  static const char* getName() {
    return "AbstractButton";
  }

  /**
   * Default constructor.
   */
  AbstractButton(Gui* gui);
  
  /**
   * Shows if the button is currently enabled.
   *
   * Exported to Lua.
   */
  virtual bool isEnabled () const;
  
  /**
   * Disables or enables the button.
   *
   * Exported to Lua.
   */
  virtual void setEnabled (bool e = true);
  
  /**
   * Callback function called when the button is clicked.
   * Exported to Lua.
   */
  virtual void onSubmit () {};
  
  //Inherited from Widget
  virtual bool onKeyPressed (sf::Event::KeyEvent key);
  virtual bool onKeyReleased (sf::Event::KeyEvent key);
  virtual bool onMouseButtonPressed (int button, float x, float y);
  virtual bool onMouseButtonReleased (int button, float x, float y);
  
 protected:
  /**
   * Shows if the button is currently pressed.
   */
  bool mPressed;
  
  /**
   * Shows if the button is currently enabled
   */
  bool mEnabled;
};
  
} // namespace
