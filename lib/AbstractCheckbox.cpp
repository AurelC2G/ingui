/**
 * inGui
 *
 * Author : aurelien.fredouelle@gmail.com
 */


#include "AbstractCheckbox.h"

using namespace std;

namespace inGui {

AbstractCheckbox::AbstractCheckbox(Gui* gui)
    : AbstractButton(gui)
    , mChecked(false) {
}

bool AbstractCheckbox::IsChecked() {
  return mChecked;
}

void AbstractCheckbox::SetChecked(bool c) {
  mChecked = c;
}

void AbstractCheckbox::onSubmit() {
  mChecked = !mChecked;
}

} // namespace
