/**
 * inGui
 *
 * Author : aurelien.fredouelle@gmail.com
 */

#pragma once

#include "AbstractButton.h"

namespace inGui {

class INGUI_API AbstractCheckbox : public AbstractButton {
 public:
  static const char* getName() {
    return "AbstractCheckbox";
  }

  /**
   * Default constructor.
   */
  AbstractCheckbox(Gui* gui);

  /**
   * Returns the checked state of the checkbox.
   *
   * Exported to Lua
   */
  virtual bool IsChecked();
  
  /**
   * Sets the checked state of the checkbox.
   *
   * Exported to Lua
   */
  virtual void SetChecked(bool c = true);
  
  //Inherited from AbstractButton
  virtual void onSubmit();
  
 protected:
  /**
   * Shows if the checkbox is currently checked
   */
  bool mChecked;
};

} // namespace
