/**
 * inGui
 *
 * Author : aurelien.fredouelle@gmail.com
 */


#include "Button.h"

#include "ColorUtils.h"
#include "Frame.h"
#include "Gui.h"
#include "Label.h"

using namespace std;

namespace inGui {

Button::Button(Gui* gui)
    : AbstractButton(gui)
    , HasColor({"text", "background"})
    , mLabel(gui->createWidget<Label>()) {
  addChild(mLabel);
  setColor("text", sf::Color::Black);
  setColor("background", sf::Color(200, 200, 200, 255));
}

const std::string& Button::getText() const {
  return mLabel->getText();
}

void Button::setText(const std::string& text) {
  mLabel->setText(text);
}

  void Button::AdjustSize ()
  {
    mLabel->Update(0); // XXX temporary, replace by signals
    SetWidth(mLabel->GetWidth() + 10);
    SetHeight(mLabel->GetHeight() + 10);
  }

  void Button::RenderWidget (sf::RenderTarget& Target) const
  {
    auto backgroundColor = getColor("background");
    sf::Color highlightColor(backgroundColor);
    sf::Color shadowColor(backgroundColor);

    if (mPressed)
      {
        ColorUtils::addOffset(backgroundColor, -0x30);
        ColorUtils::addOffset(highlightColor, -0x60);
      }
    else
      {
        ColorUtils::addOffset(highlightColor, 0x30);
        ColorUtils::addOffset(shadowColor, -0x30);
      }

    //Background
    {
      sf::Shape rect = sf::Shape::Rectangle(0, 0, GetWidth() - 1, GetHeight() - 1, backgroundColor);
      Target.Draw(rect);
    }

    //Highlight
    {
      sf::Shape line = sf::Shape::Line(0, 0, GetWidth() - 1, 0, 3, highlightColor);
      Target.Draw(line);
    }
    {
      sf::Shape line = sf::Shape::Line(0, 1, 0, GetHeight() - 1, 3, highlightColor);
      Target.Draw(line);
    }

    //Shadow
    {
      sf::Shape line = sf::Shape::Line(GetWidth() - 1, 1, GetWidth() - 1, GetHeight() - 1, 3, shadowColor);
      Target.Draw(line);
    }
    {
      sf::Shape line = sf::Shape::Line(1, GetHeight() - 1, GetWidth() - 1, GetHeight() - 1, 3, shadowColor);
      Target.Draw(line);
    }


    auto textColor = getColor("text");
    mLabel->setColor(textColor);
    if (mPressed)
      {
        mLabel->SetX((GetWidth() - mLabel->GetWidth()) / 2 + 1);
        mLabel->SetY((GetHeight() - mLabel->GetHeight()) / 2 + 1);
      }
    else
      {
        mLabel->SetX((GetWidth() - mLabel->GetWidth()) / 2);
        mLabel->SetY((GetHeight() - mLabel->GetHeight()) / 2);

        if (hasFocus())
          {
            sf::Shape rect = sf::Shape::Rectangle(3, 3, GetWidth() - 3, GetHeight() - 3, sf::Color(0, 0, 0, 0), 1, textColor);
            Target.Draw(rect);
          }
      }
  }
}
