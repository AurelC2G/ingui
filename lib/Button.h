/**
 * inGui
 *
 * Author : aurelien.fredouelle@gmail.com
 */

#pragma once

#include "AbstractButton.h"
#include "HasColor.h"

namespace inGui {

class Label;
class Frame;

class INGUI_API Button : public AbstractButton,
                         public HasColor {
 public:
  static const char* getName() {
    return "Button";
  }

  /**
   * Default constructor.
   */
  Button(Gui* gui);
  
  /**
   * Gets the text of the button.
   */
  virtual const std::string& getText() const;
  
  /**
   * Sets the text of the button.
   */
  virtual void setText(const std::string& text);
  
  /**
   * Adjusts the button's size to fit the caption.
   *
   * Exported to Lua.
   */
  virtual void AdjustSize();

 protected:
  //Inherited from Widget
  virtual void RenderWidget(sf::RenderTarget& Target) const;
  
  /**
   * Label used to display the caption
   */
  std::shared_ptr<Label> mLabel;
};

} // namespace
