/**
 * inGui
 *
 * Author : aurelien.fredouelle@gmail.com
 */


#include "Checkbox.h"

#include "ColorUtils.h"

using namespace std;

namespace inGui {

Checkbox::Checkbox(Gui* gui)
    : AbstractCheckbox(gui)
    , HasColor({"border", "background", "tick"}) {
  setColor("border", sf::Color::Black);
  setColor("background", sf::Color::White);
  setColor("tick", sf::Color::Green);
  SetSize(23, 23);
}

void Checkbox::RenderWidget (sf::RenderTarget& Target) const {
  const float h = GetHeight() - 2;
  
  //Background
  {
    sf::Shape rect = sf::Shape::Rectangle(2, 2, h - 2, h - 2, getColor("background"));
    Target.Draw(rect);
  }
  
  //Border
  auto borderColor = getColor("border");
  sf::Color highlightColor(borderColor);
  ColorUtils::addOffset(highlightColor, 0x30);
  
  sf::Color shadowColor(borderColor);
  ColorUtils::addOffset(shadowColor, -0x30);
  
  {
    sf::Shape line = sf::Shape::Line(1, 1, h, 1, 3, shadowColor);
    Target.Draw(line);
  }
  {
    sf::Shape line = sf::Shape::Line(1, 1, 1, h, 3, shadowColor);
    Target.Draw(line);
  }

  {
    sf::Shape line = sf::Shape::Line(h, 1, h, h, 3, highlightColor);
    Target.Draw(line);
  }
  {
    sf::Shape line = sf::Shape::Line(1, h - 1, h - 1, h - 1, 3, highlightColor);
    Target.Draw(line);
  }

  //Foreground
  auto tickColor = getColor("tick");
  if (mChecked) {
    {
      sf::Shape line = sf::Shape::Line(3, 6, 7, h - 2, 3, tickColor);
      Target.Draw(line);
    }
    {
      sf::Shape line = sf::Shape::Line(7, h - 3, h - 3, 3, 3, tickColor);
      Target.Draw(line);
    }
  }
}

} // namespace
