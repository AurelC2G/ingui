/**
 * inGui
 *
 * Author : aurelien.fredouelle@gmail.com
 */

#pragma once

#include "AbstractCheckbox.h"
#include "HasColor.h"

namespace inGui {

class INGUI_API Checkbox : public AbstractCheckbox,
                           public HasColor {
 public:
  static const char* getName() {
    return "Checkbox";
  }

  /**
   * Default constructor.
   */
  Checkbox(Gui* gui);
  
 protected:
  //Inherited from Widget
  virtual void RenderWidget(sf::RenderTarget& Target) const;
};

} // namespace
