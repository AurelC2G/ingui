/**
 * inGui
 *
 * Author : aurelien.fredouelle@gmail.com
 */


#include "Clipping.h"


namespace inGui
{
  ClipArea::ClipArea (float X, float Y, float Width, float Height) :
    x(X),
    y(Y),
    width(Width),
    height(Height)
  {
  }


  std::stack<ClipArea> Clipping::mAreas;

  void Clipping::Push (ClipArea area)
  {
    if (area.width <= 0 || area.height <= 0)
      {
        area.width = 0;
        area.height = 0;
      }

    if (!Clipping::mAreas.empty())
      {
        ClipArea &top = Clipping::mAreas.top();

        if (area.x < 0)
          {
            area.width += area.x;
            area.x = 0;
          }

        if (area.y < 0)
          {
            area.height += area.y;
            area.y = 0;
          }

        if (area.x + area.width > top.width)
          {
            area.width = top.width - area.x;
          }

        if (area.y + area.height > top.height)
          {
            area.height = top.height - area.y;
          }

        if (area.width <= 0 || area.height <= 0)
          {
            area.width = 0;
            area.height = 0;
          }
      }

    Clipping::mAreas.push(area);
    Clipping::ApplyClipArea(area);
  }

  void Clipping::Pop ()
  {
    if (Clipping::mAreas.empty())
      {
        return;
      }

    Clipping::mAreas.pop();

    if (Clipping::mAreas.empty())
      {
        Clipping::ClearClipRegion();
      }
    else
      {
        ClipArea area = Clipping::mAreas.top();
        area.x = 0;
        area.y = 0;
        Clipping::ApplyClipArea(area);
      }
  }

  void Clipping::ApplyClipArea (const ClipArea &area)
  {
    glEnable(GL_CLIP_PLANE0);
    glEnable(GL_CLIP_PLANE1);
    glEnable(GL_CLIP_PLANE2);
    glEnable(GL_CLIP_PLANE3);

    double plane [4]={0,0,0,0};

    plane[1] = 1;
    plane[3] = - area.y;
    glClipPlane(GL_CLIP_PLANE0, &plane[0]);

    plane[1] = -1;
    plane[3] = area.y + area.height;
    glClipPlane(GL_CLIP_PLANE1, &plane[0]);

    plane[0] = 1;
    plane[1] = 0;
    plane[3] = - area.x;
    glClipPlane(GL_CLIP_PLANE2, &plane[0]);

    plane[0] = -1;
    plane[3] = area.x + area.width;
    glClipPlane(GL_CLIP_PLANE3, &plane[0]);
  }

  void Clipping::ClearClipRegion ()
  {
    glDisable(GL_CLIP_PLANE0);
    glDisable(GL_CLIP_PLANE1);
    glDisable(GL_CLIP_PLANE2);
    glDisable(GL_CLIP_PLANE3);
  }
}
