/**
 * inGui
 *
 * Author : aurelien.fredouelle@gmail.com
 */

#pragma once

#include "Config.h"
#include <SFML/Graphics.hpp>
#include <stack>

namespace inGui
{
  struct INGUI_API ClipArea
  {
    /**
     * Default constructor
     */
    ClipArea (float X = 0, float Y = 0, float Width = 0, float Height = 0);

    float x, y, width, height;
  };

  class INGUI_API Clipping
  {
  public :
    /**
     * Push a new clipping area
     */
    static void Push (ClipArea area);

    /**
     * Pops the current clipping area
     */
    static void Pop ();

  protected:
    /**
     * Applies the given ClipArea
     */
    static void ApplyClipArea (const ClipArea &area);

    /**
     * Disables all clipping
     */
    static void ClearClipRegion ();

    /**
     * Stack of the current clip areas
     */
    static std::stack<ClipArea> mAreas;
  };
} // namespace
