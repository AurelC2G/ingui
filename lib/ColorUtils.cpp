/**
 * inGui
 *
 * Author : aurelien.fredouelle@gmail.com
 */

#include "ColorUtils.h"

namespace inGui {

void ColorUtils::addOffset(sf::Color &color, char offset) {
  if (color.r + offset > 255) {
    color.r = 255;
  } else if (color.r + offset < 0) {
    color.r = 0;
  } else {
    color.r += offset;
  }

  if (color.g + offset > 255) {
    color.g = 255;
  } else if (color.g + offset < 0) {
    color.g = 0;
  } else {
    color.g += offset;
  }

  if (color.b + offset > 255) {
    color.b = 255;
  } else if (color.b + offset < 0) {
    color.b = 0;
  } else {
    color.b += offset;
  }
}

} // namespace
