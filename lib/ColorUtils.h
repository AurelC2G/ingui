/**
 * inGui
 *
 * Author : aurelien.fredouelle@gmail.com
 */

#pragma once

#include <SFML/Graphics.hpp>

namespace inGui {

class ColorUtils {
 public:
  /**
   * Add an offset to all RGB components of a color
   */
  static void addOffset(sf::Color &color, char offset);
};

} // namespace
