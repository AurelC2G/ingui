/**
 * inGui
 *
 * Author : aurelien.fredouelle@gmail.com
 */

#pragma once

#include <SFML/Config.hpp>

////////////////////////////////////////////////////////////
// Define a portable debug macro
////////////////////////////////////////////////////////////
#ifdef NDEBUG
#  define INGUI_DEBUG
#endif

////////////////////////////////////////////////////////////
// Define portable import / export macros
////////////////////////////////////////////////////////////
#if defined(SFML_SYSTEM_WINDOWS)
#  ifdef INGUI_EXPORTS
#    define INGUI_API __declspec(dllexport)
#  else
#    define INGUI_API __declspec(dllimport)
#  endif
#else
#  define INGUI_API
#endif
