/**
 * inGui
 *
 * Author : aurelien.fredouelle@gmail.com
 */

#include "FocusManager.h"

#include "Widget.h"
#include <stack>
#include <boost/range/adaptor/reversed.hpp>

using namespace std;

namespace inGui {

FocusManager::FocusManager(Widget* root)
    : mRoot(root)
    , mFocused(nullptr)
    , mHot(nullptr) {
}

Widget* FocusManager::getFocused() const {
  return mFocused;
}

bool FocusManager::hasFocus(const Widget* widget) const {
  return widget && (widget == mFocused);
}

bool FocusManager::requestFocus(Widget* widget) {
  if (!widget) {
    return false;
  }
  
  if (mFocused == widget) {
    return true;
  }

  if (isBlockedByModal(widget)) {
    return false;
  }

  if (mFocused) {
    mFocused->dispatchEvent("onFocusLost");
  }
  mFocused = widget;
  widget->dispatchEvent("onFocusGained");
    
  return true;
}

void FocusManager::releaseFocus(Widget* widget) {
  bool shouldDispatch = widget && mFocused == widget;
  doReleaseFocus(widget);
  if (shouldDispatch) {
    widget->dispatchEvent("onFocusLost");
  }
}

bool FocusManager::hasModalFocus(const Widget* widget) const {
  if (!widget) {
    return false;
  }
  
  for (const auto it : mModalFocused) {
    if (it == widget) {
      return true;
    }
  }

  return false;
}

bool FocusManager::requestModalFocus(Widget* widget) {
  if (!widget) {
    return false;
  }

  if (isBlockedByModal(widget)) {
    return false;
  }

  // We can grant the request
  mModalFocused.push_front(widget);
    
  // Let's give a bonus : the focus !
  requestFocus(widget);// XXX really? what if a child has the focus?
    
  return true;
}

void FocusManager::releaseModalFocus(Widget* widget) {
  mModalFocused.remove(widget);
}

void FocusManager::focusNext() { // XXX this seems crappy
  //First, get the root under which we can't go
  auto root = mRoot;
  if (!mModalFocused.empty()) {
    root = mModalFocused.front();
  }

    // Then, let's find the next in the list. We search all the tree
    // since root (because it's easier and I'm lazy ;))

    Widget *firstFocusable = nullptr, *next = nullptr;
    bool currentSeen = false;
    stack<Widget*> workingStack;
    workingStack.push(root);

    while (!workingStack.empty()) {
      Widget *current = workingStack.top();
      workingStack.pop();

      if (!firstFocusable && current->isFocusable()) {
	firstFocusable = current;
      }

      if (current == mFocused) {
	currentSeen = true;
      } else if (current->isFocusable() && currentSeen) {
	//We've got it !
	next = current;
	break;
      }

      //Add children in reverse order
      for (auto it : boost::adaptors::reverse(current->getChildren())) {
	workingStack.push(it.get());
      }
    }

    if (!next) {
      next = firstFocusable;
    }

    if (next != mFocused) {
      if (mFocused) {
	mFocused->dispatchEvent("onFocusLost");
      }
      mFocused = next;
      if (next) {
	next->dispatchEvent("onFocusGained");
      }
    }
  }

void FocusManager::focusPrevious() { // XXX this seems crappy
  //First, get the root under which we can't go
  auto root = mRoot;
  if (!mModalFocused.empty()) {
    root = mModalFocused.front();
  }

    //Then, let's find the previous in the list. We search all the tree
    //since root (because it's easier and I'm lazy ;))

    Widget *bestCandidate = nullptr, *next = nullptr;
    stack<Widget*> workingStack;
    workingStack.push(root);

    while (!workingStack.empty())
      {
        Widget *current = workingStack.top();
        workingStack.pop();

        if (current == mFocused && bestCandidate)
          {
            //We've got it !
            next = bestCandidate;
            break;
          }
        else if (current->isFocusable())
          {
            bestCandidate = current;
          }

        //Add children in reverse order
	for (auto it : boost::adaptors::reverse(current->getChildren())) {
	  workingStack.push(it.get());
	}
      }

    if (!next) {
      next = bestCandidate;
    }

    if (next != mFocused) {
      if (mFocused) {
	mFocused->dispatchEvent("onFocusLost");
      }
      mFocused = next;
      if (next) {
	next->dispatchEvent("onFocusGained");
      }
    }
  }

Widget* FocusManager::getHotWidget() {
  return mHot;
}

void FocusManager::updateHotWidget(int mouseX, int mouseY) {
  auto newHot = mRoot->findHotWidget(mouseX, mouseY);
  if (newHot != mHot) {
    if (mHot) {
      mHot->dispatchEvent("onMouseOut");
    }
    if (newHot) {
      newHot->dispatchEvent("onMouseOver");
    }
    mHot = newHot;
  }
}

void FocusManager::removeWidget(Widget* widget) {
  doReleaseFocus(widget);
  if (mHot == widget) {
    mHot = nullptr;
  }
}

bool FocusManager::isBlockedByModal(const Widget* widget) const {
  if (!widget || mModalFocused.empty()) {
    return false;
  }
  
  return !widget->isParent(mModalFocused.front());
}

void FocusManager::doReleaseFocus(Widget* widget) {
  if (!widget) {
    return;
  }

  if (mFocused == widget) {
    mFocused = nullptr;
    
    // We had the focus, so if we have the modal one, we are on the top
    if (!mModalFocused.empty() && mModalFocused.front() == widget) {
      mModalFocused.pop_front();
    }
  } else {
    // We may have modal focus, we have to release it
    mModalFocused.remove(widget);
  }
}

} // namespace
