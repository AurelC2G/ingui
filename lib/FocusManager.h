/**
 * inGui
 *
 * Author : aurelien.fredouelle@gmail.com
 */

#pragma once

#include "Config.h"
#include <memory>
#include <list>

namespace inGui {

class Widget;

class INGUI_API FocusManager {
 public:
  FocusManager(Widget* root);

  Widget* getFocused() const;

  /**
   * Indicates if a widget has currently the focus.
   */
  bool hasFocus(const Widget* widget) const;
  
  /**
   * Requests focus. Returns true in case of success,
   * returns false if the request couldn't be granted
   * (a non-parent widget has modal focus).
   */
  bool requestFocus(Widget* widget);
  
  /**
   * Releases focus (including modal) from a widget
   */
  void releaseFocus(Widget* widget);
  
  /**
   * Indicates if a widget has currently the modal focus.
   */
  bool hasModalFocus(const Widget* widget) const;
  
  /**
   * Requests modal focus. Returns true in case of success,
   * returns false if the request couldn't be granted (a
   * non-parent widget has modal focus).
   */
  bool requestModalFocus(Widget* widget);
  
  /**
   * Releases modal focus, but keeps the "standard" focus.
   * If the widget hadn't the focus, no effect.
   */
  void releaseModalFocus(Widget* widget);
  
  /**
   * Gives the focus to the next focusable element in the gui.
   */
  void focusNext();
  
  /**
   * Gives the focus to the previous focusable element in the gui.
   */
  void focusPrevious();

  /**
   * Returned the widget currently hovered by the mouse
   */
  Widget* getHotWidget();
  
  /**
   * Update the widget currently hovered by the mouse
   */
  void updateHotWidget(int mouseX, int mouseY);

  /**
   * Remove all type of focus from a given widget.
   *
   * This is usually used just before the widget is removed
   * from the gui or destroyed.
   */
  void removeWidget(Widget* widget);

 private:
  bool isBlockedByModal(const Widget* widget) const;
  void doReleaseFocus(Widget* widget);

  /**
   * Root widget
   */
  Widget* mRoot;
  
  /**
   * Widget having the focus
   */
  Widget *mFocused;
  
  /**
   * Stack of modal focused widgets
   */
  std::list<Widget*> mModalFocused;

  /**
   * Widget currently hovered by the mouse
   */
  Widget* mHot;
};

} // namespace
