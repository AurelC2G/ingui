/**
 * inGui
 *
 * Author : aurelien.fredouelle@gmail.com
 */


#include "FontLoader.h"

namespace inGui
{
  FontLoader& FontLoader::Get ()
  {
    static FontLoader inst;
    return inst;
  }

  FontLoader::FontLoader ()
  {
  }

  FontLoader::~FontLoader ()
  {
  }

  std::shared_ptr<sf::Font> FontLoader::Get (const std::string &name, unsigned int size)
  {
    std::weak_ptr<sf::Font> ptr = mFonts[name][size];
    std::shared_ptr<sf::Font> retPtr = ptr.lock();

    if (retPtr)
      {
        //Font is loaded, return it
        return retPtr;
      }

    //Get the path of the font family
    std::map<std::string, std::string>::iterator path = mFamilies.find(name);
    if (path == mFamilies.end())
      {
        //Font family is not registered
        throw 1;//TODO error handling
      }

    //Load the font
    retPtr = std::shared_ptr<sf::Font>(new sf::Font());
    retPtr->LoadFromFile(path->second, size);

    //Store in the manager
    mFonts[name][size] = std::weak_ptr<sf::Font>(retPtr);

    //Return the shared ptr
    return retPtr;
  }

  void FontLoader::RegisterFont (const std::string &name, const std::string &path)
  {
    mFamilies[name] = path;
  }
}
