/**
 * inGui
 *
 * Author : aurelien.fredouelle@gmail.com
 */

#pragma once

#include "Config.h"
#include <SFML/Graphics.hpp>
#include <map>
#include <string>
#include <memory>

namespace inGui
{
  class INGUI_API FontLoader
  {
  public :
    /**
     * Gets the unique instance of FontLoader.
     */
    static FontLoader& Get ();

    /**
     * Destructor
     */
    virtual ~FontLoader ();

    /**
     * Loads if necessary, and then returns a registered font.
     */
    std::shared_ptr<sf::Font> Get (const std::string &name, unsigned int size);

    /**
     * Registers a font family.
     *
     * Exported to Lua.
     */
    void RegisterFont (const std::string &name, const std::string &path);

  protected:
    /**
     * Default constructor.
     */
    FontLoader ();

    /**
     * Registered font families
     */
    std::map<std::string, std::string> mFamilies;

    /**
     * Map of pointers to loaded fonts
     */
    std::map<std::string, std::map<unsigned int, std::weak_ptr<sf::Font> > > mFonts;
  };
} // namespace

