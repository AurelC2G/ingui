/**
 * inGui
 *
 * Author : aurelien.fredouelle@gmail.com
 */


#include "Frame.h"

#include "Gui.h"
#include "Icon.h"

using namespace std;

namespace inGui {

Frame::Frame(Gui* gui)
    : Widget(gui)
    , HasColor({"border", "background"})
    , mIcon(gui->createWidget<Icon>())
    , mBorderSize(0) {
  mIcon->SetMouseOpaque(false);
  addChild(mIcon);

  // Use a more sensible default for frame background color
  setColor("background", sf::Color::White);
}

  void Frame::SetBackground (const std::string &name)
  {
    mIcon->SetImage(name);
  }

  unsigned int Frame::GetBorderSize () const
  {
    return mBorderSize;
  }

  void Frame::SetBorder (unsigned int size, const sf::Color &color)
  {
    mBorderSize = size;
    setColor("border", color);
  }

  void Frame::Update (float time)
  {
    mIcon->SetSize(GetWidth(), GetHeight());
    mIcon->setColor(getColor("background"));
  }

  void Frame::RenderWidgetPostTreatment (sf::RenderTarget& Target) const
  {
    //Background is already drawn by mIcon

    //Draw border
    if (mBorderSize > 0)
      {
        sf::Shape rect = sf::Shape::Rectangle(mBorderSize, mBorderSize, GetWidth() - mBorderSize, GetHeight() - mBorderSize, sf::Color(0, 0, 0, 0), mBorderSize, getColor("border"));
        Target.Draw(rect);
      }
  }
}
