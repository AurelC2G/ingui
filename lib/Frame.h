/**
 * inGui
 *
 * Author : aurelien.fredouelle@gmail.com
 */

#pragma once

#include "Widget.h"
#include "HasColor.h"

namespace inGui {

class Icon;

class INGUI_API Frame : public Widget,
                        public HasColor {
 public:
  static const char* getName() {
    return "Frame";
  }

  /**
   * Default constructor.
   */
  Frame(Gui* gui);

    /**
     * Sets a background image.
     *
     * Exported to Lua.
     */
    void SetBackground (const std::string &name);

    /**
     * Gets the size of the border.
     *
     * Exported to Lua.
     */
    unsigned int GetBorderSize () const;

    /**
     * Sets the border size and color.
     *
     * Exported to Lua.
     */
    void SetBorder (unsigned int size, const sf::Color &color);

    //Inherited from Widget
    virtual void Update (float time);

  protected:
    //Inherited from Widget
    virtual void RenderWidgetPostTreatment (sf::RenderTarget& Target) const;


    /**
     * Background image
     */
    std::shared_ptr<Icon> mIcon;

    /**
     * Border size
     */
    unsigned int mBorderSize;
  };
} // namespace
