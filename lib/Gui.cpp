/**
 * inGui
 *
 * Author : aurelien.fredouelle@gmail.com
 */


#include "Gui.h"

#include "Clipping.h"
#include "FocusManager.h"
#include "FontLoader.h"
#include "ImageLoader.h"

#include "HasColor.h"

#include "Widget.h"
#include "Button.h"
#include "Checkbox.h"
#include "Frame.h"
#include "Icon.h"
#include "ImageButton.h"
#include "Label.h"
#include "PasswordField.h"
#include "Table.h"
#include "TextField.h"

#include "get_pointer.h"

#include <iostream>
#include <string>
#include <sstream>
#include <stack>

#include <luabind/out_value_policy.hpp>

using namespace std;

#define DECLARE_LUA_LIBRARY(name) \
  extern const char lua_##name[]; \
  extern const size_t lua_##name##_length;

#define LOAD_LUA_LIBRARY(name) \
  mLua.runResource(lua_##name, lua_##name##_length, "lua/#name#.lua")

DECLARE_LUA_LIBRARY(utils)
DECLARE_LUA_LIBRARY(ingui)
DECLARE_LUA_LIBRARY(HasColor)

namespace inGui {

Gui::Gui (float width, float height)
    : mRoot(Widget::createRoot()) {
  SetSize(width, height);

  //We register C++ classes into Lua
  luabind::module(mLua.getState(), "inGui") [
    //Exports from SFML
    luabind::class_<sf::Color>("Color")
      .def(luabind::constructor<>())
      .def(luabind::constructor<sf::Uint8, sf::Uint8, sf::Uint8, sf::Uint8>())
      .def_readwrite("r", &sf::Color::r)
      .def_readwrite("g", &sf::Color::g)
      .def_readwrite("b", &sf::Color::b)
      .def_readwrite("a", &sf::Color::a),

    luabind::class_<sf::Event::KeyEvent>("KeyCodes")
      .def(luabind::constructor<>())
      .enum_("Codes") [
        luabind::value("A", sf::Key::A),
        luabind::value("B", sf::Key::B),
        luabind::value("C", sf::Key::C),
        luabind::value("D", sf::Key::D),
        luabind::value("E", sf::Key::E),
        luabind::value("F", sf::Key::F),
        luabind::value("G", sf::Key::G),
        luabind::value("H", sf::Key::H),
        luabind::value("I", sf::Key::I),
        luabind::value("J", sf::Key::J),
        luabind::value("K", sf::Key::K),
        luabind::value("L", sf::Key::L),
        luabind::value("M", sf::Key::M),
        luabind::value("N", sf::Key::N),
        luabind::value("O", sf::Key::O),
        luabind::value("P", sf::Key::P),
        luabind::value("Q", sf::Key::Q),
        luabind::value("R", sf::Key::R),
        luabind::value("S", sf::Key::S),
        luabind::value("T", sf::Key::T),
        luabind::value("U", sf::Key::U),
        luabind::value("V", sf::Key::V),
        luabind::value("W", sf::Key::W),
        luabind::value("X", sf::Key::X),
        luabind::value("Y", sf::Key::Y),
        luabind::value("Z", sf::Key::Z),
        luabind::value("Num0", sf::Key::Num0),
        luabind::value("Num1", sf::Key::Num1),
        luabind::value("Num2", sf::Key::Num2),
        luabind::value("Num3", sf::Key::Num3),
        luabind::value("Num4", sf::Key::Num4),
        luabind::value("Num5", sf::Key::Num5),
        luabind::value("Num6", sf::Key::Num6),
        luabind::value("Num7", sf::Key::Num7),
        luabind::value("Num8", sf::Key::Num8),
        luabind::value("Num9", sf::Key::Num9),
        luabind::value("Escape", sf::Key::Escape),
        luabind::value("LControl", sf::Key::LControl),
        luabind::value("LShift", sf::Key::LShift),
        luabind::value("LAlt", sf::Key::LAlt),
        luabind::value("LSystem", sf::Key::LSystem),
        luabind::value("RControl", sf::Key::RControl),
        luabind::value("RShift", sf::Key::RShift),
        luabind::value("RAlt", sf::Key::RAlt),
        luabind::value("RSystem", sf::Key::RSystem),
        luabind::value("Menu", sf::Key::Menu),
        luabind::value("LBracket", sf::Key::LBracket),
        luabind::value("RBracket", sf::Key::RBracket),
        luabind::value("SemiColon", sf::Key::SemiColon),
        luabind::value("Comma", sf::Key::Comma),
        luabind::value("Period", sf::Key::Period),
        luabind::value("Quote", sf::Key::Quote),
        luabind::value("Slash", sf::Key::Slash),
        luabind::value("BackSlash", sf::Key::BackSlash),
        luabind::value("Tilde", sf::Key::Tilde),
        luabind::value("Equal", sf::Key::Equal),
        luabind::value("Dash", sf::Key::Dash),
        luabind::value("Space", sf::Key::Space),
        luabind::value("Return", sf::Key::Return),
        luabind::value("Back", sf::Key::Back),
        luabind::value("Tab", sf::Key::Tab),
        luabind::value("PageUp", sf::Key::PageUp),
        luabind::value("PageDown", sf::Key::PageDown),
        luabind::value("End", sf::Key::End),
        luabind::value("Home", sf::Key::Home),
        luabind::value("Insert", sf::Key::Insert),
        luabind::value("Delete", sf::Key::Delete),
        luabind::value("Add", sf::Key::Add),
        luabind::value("Subtract", sf::Key::Subtract),
        luabind::value("Multiply", sf::Key::Multiply),
        luabind::value("Divide", sf::Key::Divide),
        luabind::value("Left", sf::Key::Left),
        luabind::value("Right", sf::Key::Right),
        luabind::value("Up", sf::Key::Up),
        luabind::value("Down", sf::Key::Down),
        luabind::value("Numpad0", sf::Key::Numpad0),
        luabind::value("Numpad1", sf::Key::Numpad1),
        luabind::value("Numpad2", sf::Key::Numpad2),
        luabind::value("Numpad3", sf::Key::Numpad3),
        luabind::value("Numpad4", sf::Key::Numpad4),
        luabind::value("Numpad5", sf::Key::Numpad5),
        luabind::value("Numpad6", sf::Key::Numpad6),
        luabind::value("Numpad7", sf::Key::Numpad7),
        luabind::value("Numpad8", sf::Key::Numpad8),
        luabind::value("Numpad9", sf::Key::Numpad9),
        luabind::value("F1", sf::Key::F1),
        luabind::value("F2", sf::Key::F2),
        luabind::value("F3", sf::Key::F3),
        luabind::value("F4", sf::Key::F4),
        luabind::value("F5", sf::Key::F5),
        luabind::value("F6", sf::Key::F6),
        luabind::value("F7", sf::Key::F7),
        luabind::value("F8", sf::Key::F8),
        luabind::value("F9", sf::Key::F9),
        luabind::value("F10", sf::Key::F10),
        luabind::value("F11", sf::Key::F11),
        luabind::value("F12", sf::Key::F12),
        luabind::value("F13", sf::Key::F13),
        luabind::value("F14", sf::Key::F14),
        luabind::value("F15", sf::Key::F15),
        luabind::value("Pause", sf::Key::Pause),
        luabind::value("Count", sf::Key::Count)
      ]
      .def_readonly("code", &sf::Event::KeyEvent::Code)
      .def_readonly("alt", &sf::Event::KeyEvent::Alt)
      .def_readonly("control", &sf::Event::KeyEvent::Control)
      .def_readonly("shift", &sf::Event::KeyEvent::Shift),

    luabind::class_<sf::Event::MouseButtonEvent>("MouseButtons")
      .enum_("ButtonCodes") [
        luabind::value("Left", sf::Mouse::Left),
        luabind::value("Right", sf::Mouse::Right),
        luabind::value("Middle", sf::Mouse::Middle),
        luabind::value("XButton1", sf::Mouse::XButton1),
        luabind::value("XButton2", sf::Mouse::XButton2)
      ],

    //Exports from inGui
    HasColor::getLuaBindings(),
    luabind::class_<Gui>("__GuiClass")
      .def("attachWidget", &Gui::attachWidget)

      //Size
      .def("getSize", &Gui::GetSizeLua, luabind::pure_out_value(_2) + luabind::pure_out_value(_3))
      .def("getWidth", &Gui::GetWidth)
      .def("getHeight", &Gui::GetHeight)

      //Fonts
      .def("setDefaultFont", &Gui::setDefaultFont),

    luabind::class_<ImageLoader>("__ImageLoader")
      .def("setPath", (void(ImageLoader::*)(const string&)) &ImageLoader::SetPath)
      .def("setExtension", &ImageLoader::SetExtension)
      .def("register", (bool(ImageLoader::*)(const string&, const string&, unsigned int, unsigned int)) &ImageLoader::Register)
      .def("registerOne", (bool(ImageLoader::*)(const string&, const string&)) &ImageLoader::Register),

    luabind::class_<FontLoader>("__FontLoader")
      .def("registerFont", &FontLoader::RegisterFont),

    luabind::namespace_("__abstractWidgets") [
      Widget::getLuaBindings(),

      luabind::class_<AbstractButton, Widget, shared_ptr<Widget> >(AbstractButton::getName())
        .def("isEnabled", &AbstractButton::isEnabled)
        .def("setEnabled", &AbstractButton::setEnabled)
        .def("onSubmit", &AbstractButton::onSubmit),

      luabind::class_<AbstractCheckbox, AbstractButton, shared_ptr<Widget> >(AbstractCheckbox::getName())
        .def("isChecked", &AbstractCheckbox::IsChecked)
        .def("setChecked", &AbstractCheckbox::SetChecked)
    ],

    luabind::namespace_("__widgets") [
      luabind::class_<Frame, luabind::bases<Widget, HasColor>, shared_ptr<Widget> >(Frame::getName())
        .def(luabind::constructor<Gui*>())
        .def("setBackground", &Frame::SetBackground)
        .def("getBorderSize", &Frame::GetBorderSize)
        .def("setBorder", &Frame::SetBorder),

      luabind::class_<Table, Widget, shared_ptr<Widget> >(Table::getName())
        .def(luabind::constructor<Gui*>())
        .def("setNumberOfColumns", &Table::SetNumberOfColumns)
        .def("setColumnAlignment", &Table::SetColumnAlignment)
        .def("setPadding", &Table::SetPadding)
        .def("setVerticalSpacing", &Table::SetVerticalSpacing)
        .def("setHorizontalSpacing", &Table::SetHorizontalSpacing)
        .def("adjustContent", &Table::AdjustContent),

      luabind::class_<Label, luabind::bases<Widget, HasColor>, shared_ptr<Widget> >(Label::getName())
        .def(luabind::constructor<Gui*>())
        .def("getText", &Label::getText)
        .def("setText", &Label::setText)
        .def("setWidthLimit", &Label::SetWidthLimit),

      luabind::class_<Button, luabind::bases<AbstractButton, HasColor>, shared_ptr<Widget> >(Button::getName())
        .def(luabind::constructor<Gui*>())
        .def("getText", &Button::getText)
        .def("setText", &Button::setText)
        .def("adjustSize", &Button::AdjustSize),

      luabind::class_<ImageButton, luabind::bases<AbstractButton, HasColor>, shared_ptr<Widget> >(ImageButton::getName())
        .def(luabind::constructor<Gui*>())
        .def("setImage", &ImageButton::SetImage),

      luabind::class_<Checkbox, luabind::bases<AbstractCheckbox, HasColor>, shared_ptr<Widget> >(Checkbox::getName())
        .def(luabind::constructor<Gui*>()),

      luabind::class_<Icon, luabind::bases<Widget, HasColor>, shared_ptr<Widget> >(Icon::getName())
        .def(luabind::constructor<Gui*>())
        .def("setImage", &Icon::SetImage)
        .def("setFrame", &Icon::SetFrame),

      luabind::class_<TextField, luabind::bases<Widget, HasColor>, shared_ptr<Widget> >(TextField::getName())
        .def(luabind::constructor<Gui*>())
        .def("getText", &TextField::getText)
        .def("setText", &TextField::setText)
        .def("isEnabled", &TextField::isEnabled)
        .def("setEnabled", &TextField::setEnabled)
        .def("onSubmit", &TextField::onSubmit),

      luabind::class_<PasswordField, TextField, shared_ptr<Widget> >(PasswordField::getName())
        .def(luabind::constructor<Gui*>())
    ]
  ];

  //We register the Gui and the Loaders to Lua
  {
    luabind::object inGui = mLua.getGlobals()["inGui"];
    inGui["Gui"] = this;
    inGui["Image"] = &(ImageLoader::Get());
    inGui["Fonts"] = &(FontLoader::Get());
  }

  // Load lua libraries
  LOAD_LUA_LIBRARY(utils);
  LOAD_LUA_LIBRARY(ingui);
  LOAD_LUA_LIBRARY(HasColor);
}

Gui::~Gui() {
  // Free the widget tree
  mRoot.reset();
}

shared_ptr<Widget> Gui::createWidget(const char* type) {
  auto out = shared_ptr<Widget>();
  try {
    out = luabind::call_function<shared_ptr<Widget> >(
      mLua.getGlobals()["inGui"]["createWidget"], type);
  } catch (luabind::error e) {
    cerr << "Lua error: " << mLua.getError() << endl;
  }
  return out;
}

LuaState& Gui::getLuaState() {
  return mLua;
}

void Gui::attachWidget(shared_ptr<Widget> widget) {
  mRoot->addChild(widget);
}

  void Gui::Draw (sf::RenderTarget& Target) const
  {
    //Draws root widget
    Target.Draw(*mRoot);
  }

  void Gui::SetSize (float width, float height)
  {
    mRoot->SetSize(width, height);
  }

  const sf::Vector2f& Gui::GetSize () const
  {
    return mRoot->GetSize();
  }

  void Gui::GetSizeLua (float *width, float *height) const
  {
    *width = mRoot->GetWidth();
    *height = mRoot->GetHeight();
  }

  float Gui::GetWidth () const
  {
    return mRoot->GetWidth();
  }

  float Gui::GetHeight () const
  {
    return mRoot->GetHeight();
  }

void Gui::setDefaultFont (const string &name, unsigned int size) {
  mRoot->SetFont(name, size);
}

  void Gui::PushInput (const sf::Event &event)
  {
    mInputQueue.push(event);
  }

  void Gui::Logic ()
  {
    auto focusManager = mRoot->getFocusManager();

    // We handle the queued events
    while (!mInputQueue.empty()) {
      sf::Event event = mInputQueue.front();
      mInputQueue.pop();

      auto focused = focusManager->getFocused();
      auto hot = focusManager->getHotWidget();

      switch (event.Type) {
      case sf::Event::KeyPressed:
	if (focused && focused->dispatchEvent("onKeyPressed", event.Key)) {
	  break;
	}

	if (event.Key.Code == sf::Key::Tab) {
	  if (event.Key.Shift){
	    focusManager->focusPrevious();
	  } else {
	    focusManager->focusNext();
	  }
	}
        break;

      case sf::Event::KeyReleased:
	if (focused) {
	  focused->dispatchEvent("onKeyReleased", event.Key);
	}
	break;

      case sf::Event::TextEntered:
	if (focused) {
	  focused->dispatchEvent("onTextEntered", event.Text.Unicode);
	}
	break;

      case sf::Event::MouseMoved:
	//We update the mouse position
	mMousePosition.x = event.MouseMove.X;
	mMousePosition.y = event.MouseMove.Y;
	break;

      case sf::Event::MouseButtonPressed:
	if (hot) {
	  hot->requestFocus();
	  hot->dispatchMouseEvent(
            "onMouseButtonPressed",
	    event.MouseButton.Button,
	    mMousePosition.x - hot->GetAbsoluteX(),
	    mMousePosition.y - hot->GetAbsoluteY());
	}
	break;

      case sf::Event::MouseButtonReleased:
	if (hot) {
	  hot->dispatchMouseEvent(
	    "onMouseButtonReleased",
	    event.MouseButton.Button,
	    mMousePosition.x - hot->GetAbsoluteX(),
	    mMousePosition.y - hot->GetAbsoluteY());
	}
	break;

      case sf::Event::MouseWheelMoved:
	if (hot) {
	  hot->dispatchMouseEvent(
	    "onMouseWheel",
	    event.MouseWheel.Delta,
	    mMousePosition.x - hot->GetAbsoluteX(),
	    mMousePosition.y - hot->GetAbsoluteY());
	}
	break;
      case sf::Event::Resized:
      case sf::Event::LostFocus:
      case sf::Event::GainedFocus:
      case sf::Event::MouseEntered:
      case sf::Event::MouseLeft:
      case sf::Event::Closed:
      case sf::Event::JoyButtonPressed:
      case sf::Event::JoyButtonReleased:
      case sf::Event::JoyMoved:
      case sf::Event::Count:
	// TODO: implement those events
	break;
      }

      focusManager->updateHotWidget(mMousePosition.x, mMousePosition.y);
    }

    //We update all widgets
    mRoot->InternalUpdate(mClock.GetElapsedTime());
    focusManager->updateHotWidget(mMousePosition.x, mMousePosition.y);
    mClock.Reset();
  }

} // namespace
