/**
 * inGui
 *
 * Author : aurelien.fredouelle@gmail.com
 */

#pragma once

#include "LuaState.h"
#include <SFML/Graphics.hpp>
#include <list>

#include <lua.hpp>
#include <luabind/luabind.hpp>

#include <string>
#include <queue>

namespace inGui {

class FocusManager;
class Widget;

class INGUI_API Gui {
 public:
    /**
     * Default constructor.
     */
    Gui(float width, float height);

    /**
     * Destructor
     */
    ~Gui();

    /**
     * Create a new widget.
     *
     * This will make a call to Lua and instantiate the widget from the
     * Lua side.
     */
    template<class T>
    std::shared_ptr<T> createWidget();

    LuaState& getLuaState();
    
    //DISPLAY

    /**
     * Attach a widget to the root.
     *
     * @param widget Pointer to the widget to add
     */
    void attachWidget(std::shared_ptr<Widget> widget);

    /**
     * Draws the Gui
     */
    void Draw(sf::RenderTarget& Target) const;

    /**
     * Sets the size of the Gui
     */
    void SetSize(float width, float height);

    /**
     * Get the size of the gui
     *
     * @return Current size in a 2D vector
     */
    virtual const sf::Vector2f& GetSize() const;

    /**
     * Get the size of the gui.
     *
     * Exported to Lua.
     */
    virtual void GetSizeLua(float *width, float *height) const;

    /**
     * Get the width of the gui.
     * Exported to Lua.
     *
     * @return Current width
     */
    virtual float GetWidth() const;

    /**
     * Get the height of the gui.
     * Exported to Lua.
     *
     * @return Current height
     */
    virtual float GetHeight() const;

    /**
     * Sets the default font.
     * Exported to Lua.
     */
    void setDefaultFont(const std::string &name, unsigned int size);




    //LOGIC

    /**
     * Pushes a new input event to the gui
     */
    void PushInput(const sf::Event &event);

    /**
     * Handles input
     */
    void Logic();

 protected:
    std::shared_ptr<Widget> createWidget(const char* type);

    /**
     * Lua context for this gui
     */
    LuaState mLua;

    /**
     * Input queue
     */
    std::queue<sf::Event> mInputQueue;

    /**
     * Clock used to count time between two calls of Logic()
     */
    sf::Clock mClock;

    /**
     * Current position of the mouse
     */
    sf::Vector2f mMousePosition;

    /**
     * Root widget
     */
    std::shared_ptr<Widget> mRoot;
  };

template<class T>
std::shared_ptr<T> Gui::createWidget() {
  return std::dynamic_pointer_cast<T>(createWidget(T::getName()));
}

} // namespace
