/**
 * inGui
 *
 * Author : aurelien.fredouelle@gmail.com
 */

#include "HasColor.h"

using namespace std;

namespace inGui {

luabind::scope HasColor::getLuaBindings() {
  return luabind::class_<HasColor>("HasColor")
    .def("__getColor", (sf::Color (HasColor::*)(const string&) const) &HasColor::getColor)
    .def("__setColor", (void (HasColor::*)(const string&, sf::Color)) &HasColor::setColor);
}

HasColor::HasColor(const list<string>& keys) {
  for (const auto& key : keys) {
    if (key == "") {
      throw runtime_error("Cannot create a color type with an empty key");
    }
    mColors[key] = sf::Color();
  }
}

sf::Color HasColor::getColor(const string& key) const {
  return mColors.at(key);
}

void HasColor::setColor(const string& key, sf::Color color) {
  mColors.at(key);
  mColors[key] = color;
}

} // namespace
