/**
 * inGui
 *
 * Author : aurelien.fredouelle@gmail.com
 */

#pragma once

#include "Config.h"
#include <list>
#include <luabind/luabind.hpp>
#include <SFML/Graphics.hpp>
#include <unordered_map>

namespace inGui {

class INGUI_API HasColor {
 public:
  static luabind::scope getLuaBindings();

  /**
   * Constructor.
   * Takes as input the list of keys to define for this widget.
   */
  HasColor(const std::list<std::string>& keys);

  /**
   * Gets one color under a given key.
   *
   * Throws an exception if the key doesn't exist.
   */
  sf::Color getColor(const std::string& key) const;

  /**
   * Sets one color under a given key.
   *
   * Throws an exception if the key doesn't exist.
   */
  void setColor(const std::string& key, sf::Color color);
  
 private:
  /**
   * Map from keys to colors
   */
  std::unordered_map<std::string, sf::Color> mColors;
};

} // namespace
