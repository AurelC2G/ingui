/**
 * inGui
 *
 * Author : aurelien.fredouelle@gmail.com
 */

#include "HasUniqueColor.h"

using namespace std;

namespace inGui {

HasUniqueColor::HasUniqueColor(string key)
    : HasColor({key})
    , mOnlyKey(key) {
}

sf::Color HasUniqueColor::getColor() const {
  return HasColor::getColor(mOnlyKey);
}

void HasUniqueColor::setColor(sf::Color color) {
  HasColor::setColor(mOnlyKey, color);
}

} // namespace
