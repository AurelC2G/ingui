/**
 * inGui
 *
 * Author : aurelien.fredouelle@gmail.com
 */

#pragma once

#include "HasColor.h"

namespace inGui {

class INGUI_API HasUniqueColor : public HasColor {
 public:
  /**
   * Constructor.
   * Takes as input the unique color key for this widget.
   */
  HasUniqueColor(std::string key);

  /**
   * Gets the color under the unique key.
   */
  sf::Color getColor() const;

  /**
   * Sets the color under the unique key.
   */
  void setColor(sf::Color color);
  
 private:
  /**
   * If this widget only has one color, holds its key.
   * Otherwise, empty string.
   */
  std::string mOnlyKey;
};

} // namespace
