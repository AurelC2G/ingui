/**
 * inGui
 *
 * Author : aurelien.fredouelle@gmail.com
 */


#include "Icon.h"

#include "ImageLoader.h"
#include "Image.h"

using namespace std;

namespace inGui {

Icon::Icon(Gui* gui)
    : Widget(gui)
    , HasUniqueColor("background")
    , mCurrentFrame(0) {
}

void Icon::SetImage(const std::string &name) {
  mImage = ImageLoader::Get().Get(name);
  mCurrentFrame = 0;
  
  //Adjust size
  sf::Sprite sprite(*(mImage->GetImage(0).get()));
  sf::Vector2f size = sprite.GetSize();
  SetSize(size.x, size.y);
}

void Icon::SetFrame(float frame) {
  mCurrentFrame = frame;
}

void Icon::Update(float time) {
  if (mImage) {
    mImage->UpdateFrameCounter(mCurrentFrame, time);
  }
}

void Icon::RenderWidget(sf::RenderTarget& Target) const {
  sf::Sprite sprite;
  sprite.SetColor(getColor());
  if (mImage) {
    sprite.SetImage(*(mImage->GetImage(mCurrentFrame).get()));
    sprite.Resize(GetWidth(), GetHeight());//TODO or repeat ?
  } else {
    sprite.Resize(GetWidth(), GetHeight());
  }
  Target.Draw(sprite);
}

} // namespace
