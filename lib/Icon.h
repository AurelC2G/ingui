/**
 * inGui
 *
 * Author : aurelien.fredouelle@gmail.com
 */

#pragma once

#include "Widget.h"
#include "HasUniqueColor.h"

namespace inGui {

class Image;

class INGUI_API Icon : public Widget,
                       public HasUniqueColor {
 public:
  static const char* getName() {
    return "Icon";
  }

  /**
   * Default constructor.
   */
  Icon(Gui* gui);

  /**
   * Set the image.
   *
   * Exported to Lua.
   */
  void SetImage(const std::string &name);

  /**
   * Sets the current frame.
   *
   * Exported to Lua.
   */
  void SetFrame(float frame);

  //Inherited from Widget
  virtual void Update (float time);
  
 protected:
  //Inherited from Widget
  virtual void RenderWidget (sf::RenderTarget& Target) const;
  
  /**
   * Image used by this icon
   */
  std::shared_ptr<Image> mImage;

  /**
   * Current frame of the image
   */
  float mCurrentFrame;
};

} // namespace
