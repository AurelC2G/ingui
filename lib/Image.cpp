/**
 * inGui
 *
 * Author : aurelien.fredouelle@gmail.com
 */


#include "Image.h"

namespace inGui
{
  Image::Image (unsigned int fps) :
    mFps(fps)
  {
  }

  Image::~Image ()
  {
  }

  std::shared_ptr<sf::Image> Image::GetImage (unsigned int index)
  {
    if (index >= mImages.size())
      {
        throw 1;//TODO error handling
      }

    return mImages[index];
  }

  void Image::UpdateFrameCounter (float &counter, float time)
  {
    counter += time * mFps;
    float size = mImages.size();
    while (counter >= size)
      {
        counter -= size;
      }
  }

  void Image::AddImage (std::shared_ptr<sf::Image> image)
  {
    std::size_t size = mImages.size();
    mImages.resize(size + 1);
    mImages[size] = image;
  }
}
