/**
 * inGui
 *
 * Author : aurelien.fredouelle@gmail.com
 */

#pragma once

#include "Config.h"
#include <SFML/Graphics.hpp>
#include <memory>
#include <vector>

namespace inGui
{
  class INGUI_API Image
  {
  public :
    /**
     * Default constructor.
     */
    Image (unsigned int fps);

    /**
     * Destructor
     */
    virtual ~Image ();

    /**
     * Gets an image
     */
    std::shared_ptr<sf::Image> GetImage (unsigned int index);

    /**
     * Updates a frame counter.
     */
    void UpdateFrameCounter (float &counter, float time);

  protected:
    friend class ImageLoader;

    /**
     * Add an image to the vector
     */
    void AddImage (std::shared_ptr<sf::Image> image);


    /**
     * Vector of the images
     */
    std::vector<std::shared_ptr<sf::Image> > mImages;

    /**
     * Frames per second
     */
    unsigned int mFps;
  };
} // namespace

