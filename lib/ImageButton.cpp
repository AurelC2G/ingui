/**
 * inGui
 *
 * Author : aurelien.fredouelle@gmail.com
 */


#include "ImageButton.h"

#include "ColorUtils.h"
#include "Gui.h"
#include "Icon.h"

using namespace std;

namespace inGui {

ImageButton::ImageButton(Gui* gui)
    : AbstractButton(gui)
    , HasColor({"background"})
    , mIcon(gui->createWidget<Icon>()) {
  mIcon->SetMouseOpaque(false);
  addChild(mIcon);
}

void ImageButton::SetImage(const string& name) {
  mIcon->SetImage(name);
  SetSize(mIcon->GetWidth(), mIcon->GetHeight());
}

void ImageButton::RenderWidget(sf::RenderTarget& Target) const {
  auto backgroundColor = getColor("background");
  sf::Color highlightColor(backgroundColor);
  sf::Color shadowColor(backgroundColor);
  
  if (mPressed) {
    ColorUtils::addOffset(backgroundColor, -0x30);
    ColorUtils::addOffset(highlightColor, -0x60);
  } else {
    ColorUtils::addOffset(highlightColor, 0x30);
    ColorUtils::addOffset(shadowColor, -0x30);
  }

  //Background
  {
    sf::Shape rect = sf::Shape::Rectangle(0, 0, GetWidth() - 1, GetHeight() - 1, backgroundColor);
    Target.Draw(rect);
  }

  //Highlight
  {
    sf::Shape line = sf::Shape::Line(0, 0, GetWidth() - 1, 0, 3, highlightColor);
    Target.Draw(line);
  }
  {
    sf::Shape line = sf::Shape::Line(0, 1, 0, GetHeight() - 1, 3, highlightColor);
    Target.Draw(line);
  }

  //Shadow
  {
    sf::Shape line = sf::Shape::Line(GetWidth() - 1, 1, GetWidth() - 1, GetHeight() - 1, 3, shadowColor);
    Target.Draw(line);
  }
  {
    sf::Shape line = sf::Shape::Line(1, GetHeight() - 1, GetWidth() - 1, GetHeight() - 1, 3, shadowColor);
    Target.Draw(line);
  }


  if (mPressed) {
    mIcon->SetX((GetWidth() - mIcon->GetWidth()) / 2 + 1);
    mIcon->SetY((GetHeight() - mIcon->GetHeight()) / 2 + 1);
  } else {
    mIcon->SetX((GetWidth() - mIcon->GetWidth()) / 2);
    mIcon->SetY((GetHeight() - mIcon->GetHeight()) / 2);
    
    if (hasFocus()) {
      sf::Color focusColor(255 - backgroundColor.r, 255 - backgroundColor.g, 255 - backgroundColor. b, backgroundColor.a);
      sf::Shape rect = sf::Shape::Rectangle(3, 3, GetWidth() - 3, GetHeight() - 3, sf::Color(0, 0, 0, 0), 1, focusColor);
      Target.Draw(rect);
    }
  }
}

} // namespace
