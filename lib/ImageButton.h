/**
 * inGui
 *
 * Author : aurelien.fredouelle@gmail.com
 */

#pragma once

#include "AbstractButton.h"
#include "HasColor.h"

namespace inGui {

class Icon;

class INGUI_API ImageButton : public AbstractButton,
                              public HasColor {
 public:
  static const char* getName() {
    return "ImageButton";
  }

  /**
   * Default constructor.
   */
  ImageButton(Gui* gui);

  /**
   * Sets the image of the button.
   *
   * Exported to Lua.
   */
  virtual void SetImage(const std::string& name);

 protected:
  //Inherited from Widget
  virtual void RenderWidget (sf::RenderTarget& Target) const;

  /**
   * Icon used to display the image
   */
  std::shared_ptr<Icon> mIcon;
};

} // namespace
