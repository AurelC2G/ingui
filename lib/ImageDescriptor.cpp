/**
 * inGui
 *
 * Author : aurelien.fredouelle@gmail.com
 */


#include "ImageDescriptor.h"

#include <sstream>

namespace inGui
{
  ImageDescriptor::ImageDescriptor (const boost::filesystem::path &filename, const std::string &extension, unsigned int imageCount, unsigned int fps) :
    mFps(fps)
  {
    mFilenames.resize(imageCount);

    for (unsigned int i = 1; i <= imageCount; i++)
      {
        std::ostringstream oss;
        oss << filename.string() << "_" << i << extension;
        mFilenames[i-1] = oss.str();
      }
  }

  ImageDescriptor::ImageDescriptor (const boost::filesystem::path &filename) :
    mFps(1)
  {
    mFilenames.resize(1);
    mFilenames[0] = filename.string();
  }

  ImageDescriptor::~ImageDescriptor ()
  {
  }
}
