/**
 * inGui
 *
 * Author : aurelien.fredouelle@gmail.com
 */

#pragma once

#include <string>
#include <vector>
#include <boost/filesystem.hpp>

namespace inGui
{
  /**
   * Base class for all image descriptors
   */
  class ImageDescriptor
  {
  public:
    /**
     * Default constructor
     */
    ImageDescriptor (const boost::filesystem::path &filename, const std::string &extension, unsigned int imageCount, unsigned int fps);

    /**
     * Constructor to use for static images (only one image)
     */
    ImageDescriptor (const boost::filesystem::path &filename);

    /**
     * Destructor
     */
    virtual ~ImageDescriptor ();

  protected:
    friend class ImageLoader;

    /**
     * List of all the filenames
     */
    std::vector<std::string> mFilenames;

    /**
     * Frames per second
     */
    unsigned int mFps;
  };
} // namespace
