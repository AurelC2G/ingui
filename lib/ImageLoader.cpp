/**
 * inGui
 *
 * Author : aurelien.fredouelle@gmail.com
 */


#include "ImageLoader.h"

#include "Image.h"
#include "ImageDescriptor.h"

namespace inGui
{
  ImageLoader& ImageLoader::Get ()
  {
    static ImageLoader inst;
    return inst;
  }

  ImageLoader::ImageLoader ()
  {
  }

  ImageLoader::~ImageLoader ()
  {
    for (std::map<std::string, ImageDescriptor*>::iterator it = mImageDescriptors.begin();
         it != mImageDescriptors.end(); it++)
      {
        if (it->second)
          {
            delete (it->second);
          }
      }
  }

  std::shared_ptr<Image> ImageLoader::Get (const std::string &name)
  {
    std::weak_ptr<Image> ptr = mImages[name];
    std::shared_ptr<Image> retPtr = ptr.lock();

    if (retPtr)
      {
        //Image is loaded, return it
        return retPtr;
      }

    //Get the image descriptor
    ImageDescriptor *descriptor = mImageDescriptors[name];
    if (!descriptor)
      {
        //Image is not registered
        throw 1;//TODO error handling
      }

    //Load the image
    retPtr = std::shared_ptr<Image>(new Image(descriptor->mFps));
    for (std::vector<std::string>::iterator it = descriptor->mFilenames.begin();
         it != descriptor->mFilenames.end(); it++)
      {
        retPtr->AddImage(GetRaw(*it));
      }

    //Store in the manager
    mImages[name] = std::weak_ptr<Image>(retPtr);

    //Return the shared ptr
    return retPtr;
  }

  void ImageLoader::SetPath (const std::string &path)
  {
    mPath = path;
  }

  void ImageLoader::SetPath (const boost::filesystem::path &path)
  {
    mPath = path;
  }

  void ImageLoader::SetExtension (const std::string &extension)
  {
    if (!extension.empty() && extension[0] != '.')
      {
        mExtension = "." + extension;
      }
    else
      {
        mExtension = extension;
      }
  }

  bool ImageLoader::Register (const std::string &name, const std::string &filename, unsigned int imageCount, unsigned int fps)
  {
    if (mImageDescriptors[name])
      {
        return false;
      }

    mImageDescriptors[name] = new ImageDescriptor(
                                                  mPath / filename,
                                                  mExtension,
                                                  imageCount,
                                                  fps
                                                  );

    return true;
  }

  bool ImageLoader::Register (const std::string &name, const std::string &filename)
  {
    if (mImageDescriptors[name])
      {
        return false;
      }

    mImageDescriptors[name] = new ImageDescriptor(mPath / filename);

    return true;
  }

  std::shared_ptr<sf::Image> ImageLoader::GetRaw (const std::string &filename)
  {
    std::weak_ptr<sf::Image> ptr = mRawImages[filename];
    std::shared_ptr<sf::Image> retPtr = ptr.lock();

    if (retPtr)
      {
        //Image is still loaded, return it
        return retPtr;
      }

    //Load the image
    retPtr = std::shared_ptr<sf::Image>(new sf::Image);
    if (!retPtr->LoadFromFile(filename))
      {
        //Couldn't load from filesystem
        throw 1;//TODO error handling
      }

    //Store in the manager
    mRawImages[filename] = std::weak_ptr<sf::Image>(retPtr);

    //Return the shared ptr
    return retPtr;
  }
}
