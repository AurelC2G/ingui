/**
 * inGui
 *
 * Author : aurelien.fredouelle@gmail.com
 */

#pragma once

#include "Config.h"
#include <SFML/Graphics.hpp>
#include <map>
#include <string>
#include <memory>
#include <boost/filesystem.hpp>

namespace inGui
{
  class Image;
  class ImageDescriptor;

  class INGUI_API ImageLoader
  {
  public :
    /**
     * Gets the unique instance of ImageLoader.
     */
    static ImageLoader& Get ();

    /**
     * Destructor
     */
    virtual ~ImageLoader ();

    /**
     * Loads if necessary, and then returns a registered image.
     */
    std::shared_ptr<Image> Get (const std::string &name);

    /**
     * Registers the path to be used for the next registrations.
     *
     * Exported to Lua.
     */
    void SetPath (const std::string &path);

    /**
     * Registers the path to be used for the next registrations.
     */
    void SetPath (const boost::filesystem::path &path);

    /**
     * Registers the extension to be used for the next registrations.
     *
     * Exported to Lua.
     */
    void SetExtension (const std::string &extension);

    /**
     * Registers an image.
     *
     * Exported to Lua.
     *
     * @param name Identifier to be used for this image
     * @param filename Root name of all images
     * @param extension Extension of the images
     * @param imageCount Number of images in the animation
     * @param fps Number of frames to be displayed each second
     *
     * @return False if this identifier was already in use.
     */
    bool Register (const std::string &name, const std::string &filename, unsigned int imageCount, unsigned int fps);

    /**
     * Registers a static image.
     *
     * Exported to Lua.
     *
     * @param name Identifier to be used for this image
     * @param filename Name of the image file, including extension, but excluding path
     *
     * @return False if this identifier was already in use.
     */
    bool Register (const std::string &name, const std::string &filename);

  protected:
    /**
     * Default constructor.
     */
    ImageLoader ();

    /**
     * Loads if necessary, and then returns a raw sf::Image.
     */
    std::shared_ptr<sf::Image> GetRaw (const std::string &filename);


    /**
     * Path to be used for the next registrations from filesystem
     */
    boost::filesystem::path mPath;

    /**
     * Extension to be used for the next registrations from filesystem
     */
    std::string mExtension;

    /**
     * Map of the registered images
     */
    std::map<std::string, ImageDescriptor*> mImageDescriptors;

    /**
     * Map of pointers to loaded images
     */
    std::map<std::string, std::weak_ptr<Image> > mImages;

    /**
     * Map of pointers to loaded raw images
     */
    std::map<std::string, std::weak_ptr<sf::Image> > mRawImages;
  };
} // namespace
