/**
 * inGui
 *
 * Author : aurelien.fredouelle@gmail.com
 */


#include "Label.h"

using namespace std;

namespace inGui {

Label::Label(Gui* gui)
    : Widget(gui)
    , HasUniqueColor("text")
    , mWidthLimit(0) {
  SetMouseOpaque(false);
}

const string& Label::getText() const {
  return mOrigText;
}

void Label::setText(const string& text) {
  mOrigText = text;
}

void Label::SetWidthLimit(unsigned int limit) {
  mWidthLimit = limit;
}

void Label::Update(float time) {
  if (!mFont) {
    mFont = GetFont();
    if (!mFont) {
      throw runtime_error("No font defined for Label");
    }
  }

  mString.SetColor(getColor());
  mString.SetFont(*(mFont.get()));
  mString.SetSize(mFont->GetCharacterSize());
  // XXX avoid updating in every frame
  UpdateText();
}

void Label::RenderWidget(sf::RenderTarget& Target) const {
  Target.Draw(mString);
}

void Label::UpdateText() {
  if (mWidthLimit) {
    mText = "";

    //Cut the string to match the right limit
    string::size_type pos, lastPos = 0;
    int length;
    do {
      pos = mOrigText.find("\n", lastPos);
      
      if (pos != string::npos) {
	length = pos - lastPos;
      } else {
	length = mOrigText.size() - lastPos;
      }

      string sub = mOrigText.substr(lastPos, length);
      mString.SetText(sub);
      sf::FloatRect rect = mString.GetRect();
      if (rect.Right - rect.Left > mWidthLimit) {
	pos = GetCutPoint(sub);
	sub = mOrigText.substr(lastPos, pos);
	lastPos += pos + 1;
      } else {
	lastPos = pos + 1;
      }

      if (!mText.empty()) {
	mText += "\n";
      }
      mText += sub;
    } while (pos != string::npos);
  } else {
    mText = mOrigText;
  }

  mString.SetText(mText);
  sf::FloatRect rect = mString.GetRect();

  float w = rect.Right - rect.Left;
  if (mWidthLimit && w > mWidthLimit) {
    w = mWidthLimit;
  }
  SetWidth(w);

  SetHeight(rect.Bottom - rect.Top);
}

string::size_type Label::GetCutPoint(const string& str) {
  string::size_type cut = str.size();

  //If impossible, don't cut the string
  if (cut == 0 || !mWidthLimit) {
    return cut;
  }

  float size = SubTextSize(str, cut);
  if (size <= mWidthLimit) {
    return cut;
  }

  //The string is too long. Let's go to the approximative cut point.
  string::size_type maxCut = cut;
  cut = ceil(cut * mWidthLimit / size);

  //Search next space
  cut = str.find(" ", cut);

  //If the position is greater than maxCut, we already know it won't work.
  if (cut != string::npos &&
      cut < maxCut &&
      SubTextSize(str, cut) <= mWidthLimit) {
    return cut;
  }

  //It still doesn't work, which means we tried too far.
  //Just try with previous space while it doesn't work.
  do {
    cut = str.rfind(" ", cut - 1);
  } while (cut > 0 && SubTextSize(str, cut) > mWidthLimit);
  
  //If cut is null, the cut failed (for example, no spaces)
  if (cut == 0) {
    return str.size();
  } else {
    return cut;
  }
}

float Label::SubTextSize(const string& str, string::size_type cut) {
  mString.SetText(str.substr(0, cut));
  sf::FloatRect rect = mString.GetRect();
  return rect.Right - rect.Left;
}

} // namespace
