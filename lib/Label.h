/**
 * inGui
 *
 * Author : aurelien.fredouelle@gmail.com
 */

#pragma once

#include "Widget.h"
#include "HasUniqueColor.h"

namespace inGui {

class INGUI_API Label : public Widget,
                        public HasUniqueColor {
 public:
  static const char* getName() {
    return "Label";
  }

  /**
   * Default constructor.
   */
  Label(Gui* gui);

  /**
   * Gets the text of the label.
   */
  virtual const std::string& getText() const;

  /**
   * Sets the text of the label.
   */
  virtual void setText(const std::string& text);

  /**
   * Sets a width limit. The text will be cut to respect it.
   *
   * Set the limit to 0 to disable this functionnality.
   *
   * Exported to Lua.
   */
  virtual void SetWidthLimit(unsigned int limit);

  //Inherited from Widget
  virtual void Update(float time);

 protected:
  //Inherited from Widget
  virtual void RenderWidget(sf::RenderTarget& Target) const;

  /**
   * Recreate the text from the original text
   */
  virtual void UpdateText();

  /**
   * Returns the position before which we have to cut the string
   * to match the width limit needs.
   */
  virtual std::string::size_type GetCutPoint(const std::string& str);

  /**
   * Returns the width of the substring [0,cut]
   */
  float SubTextSize(const std::string& str, std::string::size_type cut);


  /**
   * Width limit.
   */
  unsigned int mWidthLimit;

  /**
   * Text of the label
   */
  std::string mText;

  /**
   * Original text of the label, before any automatic cut.
   */
  std::string mOrigText;

  /**
   * SFML object used to display the string
   */
  sf::String mString;
};

} // namespace
