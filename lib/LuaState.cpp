/**
 * inGui
 *
 * Author : aurelien.fredouelle@gmail.com
 */

#include "LuaState.h"

using namespace std;

namespace inGui {

LuaState::LuaState()
    : mState(lua_open()) {
  luaL_openlibs(mState);
  luabind::open(mState);
}

LuaState::~LuaState() {
  lua_close(mState);
}

void LuaState::runString(const char str[]) {
  if (luaL_dostring(mState, str) != 0) {
    onError();
  }
}

void LuaState::runFile(const char filename[]) {
  if (luaL_dofile(mState, filename) != 0) {
    onError();
  }
}

void LuaState::runResource(const char buffer[],
			   const size_t size,
			   const char name[]) {
  auto status = luaL_loadbuffer(mState, buffer, size, name);
  if (status == 0) {
    status = lua_pcall(mState, 0, 0, 0);
  }
  if (status != 0) {
    onError();
  }
}

luabind::object LuaState::getGlobals() {
  return luabind::globals(mState);
}

lua_State* LuaState::getState() {
  return mState;
}

string LuaState::getError() {
  return string(lua_tostring(mState, -1));
}

void LuaState::onError() {
  cerr << "Lua error: " << getError() << endl;
}

} // namespace
