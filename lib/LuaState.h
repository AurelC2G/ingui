/**
 * inGui
 *
 * Author : aurelien.fredouelle@gmail.com
 */

#pragma once

#include "Config.h"

#include <lua.hpp>
#include <luabind/luabind.hpp>

namespace inGui {

class INGUI_API LuaState {
 public:
  /**
   * Constructor.
   *
   * Instantiate and initialize its own Lua state.
   */
  LuaState();

  /**
   * Destructor
   */
  ~LuaState();

  /**
   * Run a Lua string
   */
  void runString(const char str[]);

  /**
   * Run a Lua file
   *
   * The file can either contain a Lua script, or compiled Lua code.
   */
  void runFile(const char filename[]);

  /**
   * Run a Lua script loaded into memory.
   *
   * The script can either be text, or compiled Lua code.
   * @param buffer Pointer to the script
   * @param size Length of the buffer
   * @param name Name of the resource
   */
  void runResource(const char buffer[],
		   const size_t size,
		   const char name[]);

  /**
   * Access to the global Lua namespace
   */
  luabind::object getGlobals();

  /**
   * Returns the Lua state
   */
  lua_State* getState();

  /**
   * Returns the top of the stack as a string.
   *
   * Should only be called on error conditions, otherwise has
   * an undefined behavior.
   */
  std::string getError();

 private:
  /**
   * Method called whenever Lua throws an exception
   */
  void onError();


  /**
   * Lua state we hold
   */
  lua_State* mState;
};

} // namespace
