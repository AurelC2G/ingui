/**
 * inGui
 *
 * Author : aurelien.fredouelle@gmail.com
 */

#include "PasswordField.h"

#include "Label.h"

using namespace std;

namespace inGui {

PasswordField::PasswordField(Gui* gui)
    : TextField(gui) {
}

void PasswordField::Update(float time) {
  if (mNeedUpdate) {
    TextField::Update(time);
    
    wstring::size_type n = mText.size();
    mDisplayedText.resize(n);
    for (wstring::size_type i = 0; i < n; i++) {
      mDisplayedText[i] = L'*';
    }
    mLabel->setText(mDisplayedText);
    FixScroll();
  } else {
    TextField::Update(time);
  }
}

} // namespace
