/**
 * inGui
 *
 * Author : aurelien.fredouelle@gmail.com
 */

#pragma once

#include "TextField.h"

namespace inGui {

class SFML_API PasswordField : public TextField {
 public:
  static const char* getName() {
    return "PasswordField";
  }

  PasswordField(Gui* gui);

  //Inherited from TextField
  virtual void Update(float time);
};

} // namespace
