/**
 * inGui
 *
 * Author : aurelien.fredouelle@gmail.com
 */

#include "Table.h"

using namespace std;

namespace inGui {

Table::Table(Gui* gui)
    : Widget(gui)
    , mNumberOfColumns(1)
    , mNumberOfRows(1)
    , mPaddingLeft(0)
    , mPaddingRight(0)
    , mPaddingTop(0)
    , mPaddingBottom(0)
    , mVerticalSpacing(0)
    , mHorizontalSpacing(0) {
  SetMouseOpaque(false);
  mColumnWidths.push_back(0);
  mRowHeights.push_back(0);
}

void Table::SetNumberOfColumns(unsigned int numberOfColumns) {
  mNumberOfColumns = numberOfColumns;
  
  if (mColumnAlignment.size() < numberOfColumns) {
    while (mColumnAlignment.size() < numberOfColumns) {
      mColumnAlignment.push_back(Widget::HCENTER);
    }
  } else {
    while (mColumnAlignment.size() >  numberOfColumns) {
      mColumnAlignment.pop_back();
    }
  }
}

void Table::SetColumnAlignment(unsigned int column, Widget::HAlignment alignment) {
  if (column < mColumnAlignment.size()) {
    mColumnAlignment[column] = alignment;
  }
}

void Table::SetPadding(float paddingLeft,
		       float paddingRight,
		       float paddingTop,
		       float paddingBottom) {
  mPaddingLeft = paddingLeft;
  mPaddingRight = paddingRight;
  mPaddingTop = paddingTop;
  mPaddingBottom = paddingBottom;
}

void Table::SetVerticalSpacing(float verticalSpacing) {
  mVerticalSpacing = verticalSpacing;
}

void Table::SetHorizontalSpacing(float horizontalSpacing) {
  mHorizontalSpacing = horizontalSpacing;
}

void Table::AdjustContent() {
  AdjustSize();
  
  float y = mPaddingTop;
  unsigned int col = 0, row = 0;
  for (auto child : mChildren) {
    float basex = mPaddingLeft;
    for (unsigned int j = 0; j < col; j++) {
      basex += mColumnWidths[j] + mHorizontalSpacing;
    }
    
    switch (mColumnAlignment[col]) {
    case Widget::LEFT:
      child->SetX(basex);
      break;
    case Widget::HCENTER:
      child->SetX(basex + (mColumnWidths[col] - child->GetWidth()) / 2);
      break;
    case Widget::RIGHT:
      child->SetX(basex + mColumnWidths[col] - child->GetWidth());
      break;
    }
    
    child->SetY(y + (mRowHeights[row] - child->GetHeight()) / 2);
    
    col++;
    if (col == mNumberOfColumns) {
      col = 0;
      y += mRowHeights[row] + mVerticalSpacing;
      row++;
    }
  }
}

void Table::addChild(shared_ptr<Widget> Widget) {
  Widget::addChild(Widget);
  AdjustContent();
}

void Table::Update (float time) {
  AdjustContent();
}

void Table::AdjustSize () {
  mNumberOfRows = mChildren.size() / mNumberOfColumns +
    mChildren.size() % mNumberOfColumns;
  
  mColumnWidths.clear();
  
  for (unsigned int i = 0; i < mNumberOfColumns; i++) {
    mColumnWidths.push_back(0);
  }

  mRowHeights.clear();
  
  for (unsigned int i = 0; i < mNumberOfRows; i++) {
    mRowHeights.push_back(0);
  }

  unsigned int col = 0, row = 0;
  for (auto child : mChildren) {
    if (child->GetWidth() > mColumnWidths[col]) {
      mColumnWidths[col] = child->GetWidth();
    }
    if (child->GetHeight() > mRowHeights[row]) {
      mRowHeights[row] = child->GetHeight();
    }

    col++;
    if (col == mNumberOfColumns) {
      col = 0;
      row++;
    }
  }
  
  float width = mPaddingLeft;
  
  for (unsigned int i = 0; i < mNumberOfColumns; i++) {
    width += mColumnWidths[i] + mHorizontalSpacing;
  }
  
  width -= mHorizontalSpacing;
  width += mPaddingRight;
  
  float height = mPaddingTop;
  
  for (unsigned int i = 0; i < mNumberOfRows; i++) {
    height += mRowHeights[i] + mVerticalSpacing;
  }

  height -= mVerticalSpacing;
  height += mPaddingBottom;
  
  SetHeight(height);
  SetWidth(width);
}

} // namespace
