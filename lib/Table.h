/**
 * inGui
 *
 * Author : aurelien.fredouelle@gmail.com
 */

#pragma once

#include "Widget.h"

namespace inGui {

class Icon;

/**
 * Self-adjusting container class. Tables are an easy way
 * to have inGui position a group of widgets for you. It
 * organizes elements in a table layout, with fixed columns
 * and variable rows.
 * The user specifies :
 *
 * @verbatim
 * <ul>
 *   <li>the number of columns</li>
 *   <li>horizontal spacing between columns</li>
 *   <li>vertical spacing between rows</li>
 *   <li>padding around the sides of the container</li>
 *   <li>each column's alignment</li>
 * </ul>
 * @endverbatim
 */
class INGUI_API Table : public Widget {
 public:
  static const char* getName() {
    return "Table";
  }

  /**
   * Default constructor.
   */
  Table(Gui* gui);
  
  /**
   * Set the number of columns to divide the widgets into.
   * The number of rows is derived automatically from the number
   * of widgets based on the number of columns.  Default column
   * alignment is left.
   *
   * Exported to Lua.
   *
   * @param numberOfColumns the number of columns.
   */
  virtual void SetNumberOfColumns(unsigned int numberOfColumns);

  /**
   * Set a specific column's alignment.
   *
   * Exported to Lua.
   *
   * @param column the column number, starting from 0.
   * @param alignment the column's alignment. See Widget for alignments enum.
   */
  virtual void SetColumnAlignment(unsigned int column, Widget::HAlignment alignment);

  /**
   * Set the padding for the sides of the container.
   *
   * Exported to Lua.
   *
   * @param paddingLeft left padding.
   * @param paddingRight right padding.
   * @param paddingTop top padding.
   * @param paddingBottom bottom padding.
   */
  virtual void SetPadding(float paddingLeft,
			   float paddingRight,
			   float paddingTop,
			   float paddingBottom);
  
  /**
   * Set the spacing between rows.
   *
   * Exported to Lua.
   *
   * @param verticalSpacing spacing in pixels.
   */
  virtual void SetVerticalSpacing(float verticalSpacing);

  /**
   * Set the horizontal spacing between columns.
   *
   * Exported to Lua.
   *
   * @param horizontalSpacing spacing in pixels.
   */
  virtual void SetHorizontalSpacing(float horizontalSpacing);
  
  /**
   * Rearrange the widgets and resize the container.
   *
   * Exported to Lua.
   */
  virtual void AdjustContent();
  
  //Inherited from Widget
  virtual void addChild(std::shared_ptr<Widget> Widget);
  virtual void Update(float time);

 protected:
  /**
   * Adjust the size of the container to fit all the widgets.
   */
  virtual void AdjustSize();
  
  std::vector<float> mColumnWidths;
  std::vector<Widget::HAlignment> mColumnAlignment;
  std::vector<float> mRowHeights;
  unsigned int mNumberOfColumns;
  unsigned int mNumberOfRows;
  float mPaddingLeft;
  float mPaddingRight;
  float mPaddingTop;
  float mPaddingBottom;
  float mVerticalSpacing;
  float mHorizontalSpacing;
};

} // namespace
