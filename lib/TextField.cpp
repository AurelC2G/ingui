/**
 * inGui
 *
 * Author : aurelien.fredouelle@gmail.com
 */


#include "TextField.h"

#include "ColorUtils.h"
#include "Gui.h"
#include "Label.h"

using namespace std;


namespace inGui {

TextField::TextField(Gui* gui)
    : Widget(gui)
    , HasColor({"text", "background"})
    , mEnabled(true)
    , mLabel(gui->createWidget<Label>())
    , mCaretPosition(0)
    , mScrollX(0) {
  addChild(mLabel);
  
  setFocusable(true);
  setColor("text", sf::Color(0, 0, 0, 255));
  setColor("background", sf::Color(220, 220, 220, 255));
}

// XXX changing the font of the TextField should change the font of
// the embedded label

const string& TextField::getText () const {
  return mText;
}

void TextField::setText(const string& text) {
  //Cut before first line break
  string::size_type pos = text.find("\n");
  if (pos == string::npos) {
    mText = text;
    pos = text.size();
  } else {
    mText = text.substr(0, pos);
  }

  if (pos < mCaretPosition) {
    mCaretPosition = pos;
  }
  
  mNeedUpdate = true;
}

bool TextField::isEnabled() const {
  return mEnabled;
}

void TextField::setEnabled(bool e) {
  mEnabled = e;
  setFocusable(e);
}

bool TextField::onKeyPressed(sf::Event::KeyEvent key) {
  if (!mEnabled || key.Code == sf::Key::Tab) {
    return false;
  }

  if (key.Code == sf::Key::Left) {
    if (mCaretPosition > 0) {
      mCaretPosition--;
    }
  } else if (key.Code == sf::Key::Right) {
    if (mCaretPosition < mDisplayedText.size()) {
      mCaretPosition++;
    }
  } else if (key.Code == sf::Key::Delete) {
    if (mCaretPosition < mDisplayedText.size()) {
      mText.erase(mCaretPosition, 1);
      mNeedUpdate = true;
    }
  } else if (key.Code == sf::Key::Back) {
    if (mCaretPosition > 0) {
      mText.erase(mCaretPosition - 1, 1);
      mCaretPosition--;
      mNeedUpdate = true;
    }
  } else if (key.Code == sf::Key::Return) {
    try {
      luabind::object callback = mLuaObject["onSubmit"];
      luabind::call_function<void>(callback, mLuaObject);
    } catch (luabind::error e) {
      cout << "Lua error : " << lua_tostring(e.state(), -1) << endl;
    }
  } else if (key.Code == sf::Key::Home) {
    mCaretPosition = 0;
  } else if (key.Code == sf::Key::End) {
    mCaretPosition = mText.size();
  } else if ((key.Code >= 32 && key.Code <= 126) || key.Code >= 162 || key.Code == 0) {
    return false;
  }
  
  FixScroll();
  return true;
}

bool TextField::onTextEntered(sf::Uint32 key) {
  if ((key >= 32 && key <= 126) || (key >= 162)) {
    mText.insert(mCaretPosition, string(1, (char)key));
    mCaretPosition++;
    mNeedUpdate = true;
    
    return true;
  } else {
    return false;
  }
}

bool TextField::onMouseButtonPressed(int button, float x, float y) {
  if (button == sf::Mouse::Left) {
    //Taille approximative d'un caractère ?
    float limit;
    {
      sf::String str;
      str.SetFont(*(mFont.get()));
      str.SetSize(mFont->GetCharacterSize());
      str.SetText("a");
      sf::FloatRect rect = str.GetRect();
      limit = (x-2) + mScrollX - 0.6 * (rect.Right - rect.Left);
    }

    string::size_type textSize = mDisplayedText.size();
    mCaretPosition = textSize;

    for (string::size_type i = 0; i < textSize; i++) {
      sf::String str;
      str.SetFont(*(mFont.get()));
      str.SetSize(mFont->GetCharacterSize());
      str.SetText(mDisplayedText.substr(0, i));
      
      sf::FloatRect rect = str.GetRect();
      
      if (rect.Right - rect.Left > limit) {
	mCaretPosition = i;
	break;
      }
    }
    
    FixScroll();
  }
  
  return true;
}

void TextField::Update(float time) {
  if (!mFont) {
    mFont = GetFont();
    if (!mFont) {
      throw runtime_error("No font defined for TextField");
    }
  }
  
  if (mNeedUpdate) {
    mNeedUpdate = false;
    mDisplayedText = mText;
    mLabel->setText(mDisplayedText);
    FixScroll();
  }

  if (mEnabled) {
    mLabel->setColor(getColor("text"));
  } else {
    auto color = getColor("text");
    ColorUtils::addOffset(color, -0x60);
    mLabel->setColor(color);
  }
}

float TextField::GetCaretPosition() const {
  if (!mFont) {
    return 0.;
  }

  sf::String str;
  str.SetFont(*(mFont.get()));
  str.SetSize(mFont->GetCharacterSize());
  str.SetText(mDisplayedText.substr(0, mCaretPosition));
  
  sf::FloatRect rect = str.GetRect();
  return rect.Right - rect.Left;
}

void TextField::FixScroll() {
  if (hasFocus()) {
    float caretX = GetCaretPosition();
    
    if (caretX - mScrollX >= GetWidth() - 4) {
      mScrollX = caretX - GetWidth() + 4;
    } else if (caretX - mScrollX <= 0) {
      mScrollX = caretX - GetWidth() / 2;
      
      if (mScrollX < 0) {
	mScrollX = 0;
      }
    }
  }
  
  mLabel->SetPosition(2 - mScrollX, (GetHeight() - mLabel->GetHeight()) / 2);
}

void TextField::RenderWidget(sf::RenderTarget& Target) const {
  //Background
  auto backgroundColor = getColor("background");
  if (!mEnabled) {
    ColorUtils::addOffset(backgroundColor, -0x60);
  }

  sf::Shape rect = sf::Shape::Rectangle(0, 0, GetWidth() - 1, GetHeight() - 1, backgroundColor);
  Target.Draw(rect);
}

void TextField::RenderWidgetPostTreatment(sf::RenderTarget& Target) const {
  auto backgroundColor = getColor("background");
  if (!mEnabled) {
    ColorUtils::addOffset(backgroundColor, -0x60);
  }

  sf::Color highlightColor(backgroundColor);
  sf::Color shadowColor(backgroundColor);
  
  ColorUtils::addOffset(highlightColor, 0x30);
  ColorUtils::addOffset(shadowColor, -0x30);

  //Border
  {
    sf::Shape line = sf::Shape::Line(0, 0, GetWidth() - 1, 0, 2, shadowColor);
    Target.Draw(line);
  }
  {
      sf::Shape line = sf::Shape::Line(0, 1, 0, GetHeight() - 2, 2, shadowColor);
      Target.Draw(line);
  }
  {
    sf::Shape line = sf::Shape::Line(GetWidth() - 1, 1, GetWidth() - 1, GetHeight() - 1, 2, highlightColor);
    Target.Draw(line);
  }
  {
    sf::Shape line = sf::Shape::Line(1, GetHeight() - 1, GetWidth() - 1, GetHeight() - 1, 2, highlightColor);
    Target.Draw(line);
  }

  //Caret
  if (hasFocus()) {
    float x = GetCaretPosition() - mScrollX + 2;
    sf::Shape line = sf::Shape::Line(x, 3, x, GetHeight() - 4, 2, getColor("text"));
    Target.Draw(line);
  }
}

} // namespace
