/**
 * inGui
 *
 * Author : aurelien.fredouelle@gmail.com
 */

#pragma once

#include "Widget.h"
#include "HasColor.h"

namespace inGui {

class Label;

class INGUI_API TextField : public Widget,
                            public HasColor {
 public:
  static const char* getName() {
    return "TextField";
  }

  /**
   * Default constructor.
   */
  TextField(Gui* gui);

  /**
   * Gets the text of the field.
   */
  virtual const std::string& getText() const;

  /**
   * Sets the text of the field.
   */
  virtual void setText(const std::string& text);

  /**
   * Shows if the text field is currently enabled.
   *
   * Exported to Lua.
   */
  virtual bool isEnabled () const;

  /**
   * Disables or enables the text field.
   *
   * Exported to Lua.
   */
  virtual void setEnabled (bool e = true);

  /**
   * Callback function called when Enter is pressed
   * inside the text field.
   * Exported to Lua.
   */
  virtual void onSubmit () {};

  //Inherited from Widget
  virtual bool onKeyPressed (sf::Event::KeyEvent key);
  virtual bool onTextEntered (sf::Uint32 key);
  virtual bool onMouseButtonPressed (int button, float x, float y);
  virtual void Update (float time);

 protected:
  /**
   * Returns the carret position. Doesn't take scrolling
   * into account.
   */
  float GetCaretPosition () const;

  /**
   * Recomputes the horizontal scrolling
   */
  void FixScroll ();

  //Inherited from Widget
  virtual void RenderWidget (sf::RenderTarget& Target) const;
  virtual void RenderWidgetPostTreatment (sf::RenderTarget& Target) const;

  /**
   * Shows if the text field is currently enabled
   */
  bool mEnabled;

  /**
   * Current text of the widget
   */
  std::string mText;

  /**
   * If true, we have to update the displayed text
   */
  bool mNeedUpdate;

  /**
   * Text currently displayed by the widget (can be different from the real
   * text, for example for password fields)
   */
  std::string mDisplayedText;

  /**
   * Label used to display the text
   */
  std::shared_ptr<Label> mLabel;

  /**
   * Current caret position in the text
   */
  std::string::size_type mCaretPosition;

  /**
   * Current horizontal scrolling
   */
  float mScrollX;
};

} // namespace
