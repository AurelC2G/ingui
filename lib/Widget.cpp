/**
 * inGui
 *
 * Author : aurelien.fredouelle@gmail.com
 */

#include "Widget.h"

#include "Clipping.h"
#include "FocusManager.h"
#include "FontLoader.h"
#include "Gui.h"
#include "get_pointer.h"

#include <boost/range/adaptor/reversed.hpp>
#include <luabind/out_value_policy.hpp>

using namespace std;

namespace inGui {

void getSize(const Widget* widget, float *width, float *height) {
  const auto& v = widget->GetSize();
  *width = v.x;
  *height = v.y;
}

void getPosition(const Widget* widget, float* x, float* y) {
  const auto& v = widget->GetPosition();
  *x = v.x;
  *y = v.y;
}

void getAbsolutePosition(const Widget* widget, float* x, float* y) {
  *x = widget->GetAbsoluteX();
  *y = widget->GetAbsoluteY();
}

Widget::Widget(Gui* gui)
    : Widget() {
}

Widget::Widget()
    : sf::Drawable()
    , mParent(nullptr)
    , mFocusable(false)
    , mMouseOpaque(true) {
}

shared_ptr<Widget> Widget::createRoot() {
  auto root = shared_ptr<Widget>(new Widget());
  root->mFocusManager =
    shared_ptr<FocusManager>(new FocusManager(root.get()));

  return root;
}

Widget::~Widget () {
  // Detach all the children of this widget.
  // This is needed because there might be other references to those children,
  // thus they might survive the death of this widget.
  {
    auto children = mChildren;
    for (auto child : children) {
      child->detach();
    }
  }

  removeFromGui();
}

luabind::scope Widget::getLuaBindings() {
  return luabind::class_<Widget, shared_ptr<Widget> >("Widget")
    .def(luabind::constructor<Gui*>())
    .def("registerLuaObject", &Widget::registerLuaObject)

    .enum_("Align") [
      luabind::value("Center", Widget::HCENTER),
      luabind::value("Left", Widget::LEFT),
      luabind::value("Right", Widget::RIGHT),
      luabind::value("Top", Widget::TOP),
      luabind::value("Bottom", Widget::BOTTOM)
    ]

    // Size
    .def("getSize", &inGui::getSize, luabind::pure_out_value(_2) + luabind::pure_out_value(_3))
    .def("getWidth", &Widget::GetWidth)
    .def("getHeight", &Widget::GetHeight)
    .def("setSize", (void(Widget::*)(float, float)) &Widget::SetSize)
    .def("setWidth", &Widget::SetWidth)
    .def("setHeight", &Widget::SetHeight)

    // Position
    .def("getPosition", &inGui::getPosition, luabind::pure_out_value(_2) + luabind::pure_out_value(_3))
    .def("getX", &Widget::GetX)
    .def("getY", &Widget::GetY)
    .def("getAbsolutePosition", &inGui::getAbsolutePosition, luabind::pure_out_value(_2) + luabind::pure_out_value(_3))
    .def("getAbsoluteX", &Widget::GetAbsoluteX)
    .def("getAbsoluteY", &Widget::GetAbsoluteY)
    .def("setPosition", (void(sf::Drawable::*)(float, float)) &sf::Drawable::SetPosition)
    .def("setX", &sf::Drawable::SetX)
    .def("setY", &sf::Drawable::SetY)

    // Alignment
    .def("setHAlign", &Widget::SetHAlign)
    .def("setVAlign", &Widget::SetVAlign)

    // Font
    .def("setFont", (void(Widget::*)(const string &, unsigned int)) &Widget::SetFont)

    // Focusability
    .def("isFocusable", &Widget::isFocusable)
    .def("setFocusable", &Widget::setFocusable)

    // Mouse opacity
    .def("isMouseOpaque", &Widget::IsMouseOpaque)
    .def("setMouseOpaque", &Widget::SetMouseOpaque)

    // Focus
    .def("hasFocus", &Widget::hasFocus)
    .def("requestFocus", &Widget::requestFocus)
    .def("releaseFocus", &Widget::releaseFocus)
    .def("hasModalFocus", &Widget::hasModalFocus)
    .def("requestModalFocus", &Widget::requestModalFocus)
    .def("releaseModalFocus", &Widget::releaseModalFocus)

    // Attachment
    .def("add", &Widget::addChild)
    .def("detach", &Widget::detach)

    // Callbacks
    .def("onKeyPressed", &Widget::onKeyPressed)
    .def("onKeyReleased", &Widget::onKeyReleased)
    .def("onTextEntered", &Widget::onTextEntered)
    .def("onMouseButtonPressed", &Widget::onMouseButtonPressed)
    .def("onMouseButtonReleased", &Widget::onMouseButtonReleased)
    .def("onMouseWheel", &Widget::onMouseWheel)
    .def("onMouseOver", &Widget::onMouseOver)
    .def("onMouseOut", &Widget::onMouseOut)
    .def("onFocusGained", &Widget::onFocusGained)
    .def("onFocusLost", &Widget::onFocusLost);
}

void Widget::registerLuaObject(luabind::object obj) {
  mLuaObject = obj;
}

const sf::Vector2f& Widget::GetSize () const {
  return mSize;
}

float Widget::GetWidth () const {
  return mSize.x;
}

float Widget::GetHeight () const {
  return mSize.y;
}

void Widget::SetSize (float Width, float Height) {
  SetWidth(Width);
  SetHeight(Height);
}

void Widget::SetWidth (float Width) {
  mSize.x = Width;
}

void Widget::SetHeight (float Height) {
  mSize.y = Height;
}

float Widget::GetX () const {
  return GetPosition().x;
}

float Widget::GetY () const {
  return GetPosition().y;
}

float Widget::GetAbsoluteX () const {
  if (mParent) {
    return mParent->GetAbsoluteX() + GetX();
  } else {
    return GetX();
  }
}

float Widget::GetAbsoluteY () const {
  if (mParent) {
    return mParent->GetAbsoluteY() + GetY();
  } else {
    return GetY();
  }
}

void Widget::SetHAlign (HAlignment a) {
  if (!mParent) {
    return;
  }

  if (a == HCENTER) {
    SetX((mParent->GetWidth() - GetWidth()) / 2);
  } else if (a == LEFT) {
    SetX(0);
  } else if (a == RIGHT) {
    SetX(mParent->GetWidth() - GetWidth());
  }
}

void Widget::SetVAlign (VAlignment a) {
  if (!mParent) {
    return;
  }

  if (a == VCENTER) {
    SetY((mParent->GetHeight() - GetHeight()) / 2);
  } else if (a == TOP) {
    SetY(0);
  } else if (a == BOTTOM) {
    SetY(mParent->GetHeight() - GetHeight());
  }
}

shared_ptr<sf::Font> Widget::GetFont () const {
  if (mFont) {
    return mFont;
  } else if (mParent) {
    return mParent->GetFont();
  } else {
    return nullptr;
  }
}

void Widget::SetFont (shared_ptr<sf::Font> font) {
  mFont = font;
}

void Widget::SetFont (const string &name, unsigned int size) {
  mFont = FontLoader::Get().Get(name, size);
}

bool Widget::isFocusable () const {
  return mFocusable;
}

void Widget::setFocusable (bool e) {
  mFocusable = e;
  
  if (!e && hasFocus()) {
    //We cannot keep the focus
    releaseFocus();
  }
}

bool Widget::IsMouseOpaque() const {
  return mMouseOpaque;
}

void Widget::SetMouseOpaque(bool e) {
  mMouseOpaque = e;
}

bool Widget::hasFocus() const {
  auto focusManager = getFocusManager();
  return focusManager && focusManager->hasFocus(this);
}

bool Widget::requestFocus() {
  if (!mFocusable) {
    return false;
  }
  
  auto focusManager = getFocusManager();
  return focusManager && focusManager->requestFocus(this);
}

void Widget::releaseFocus() {
  auto focusManager = getFocusManager();
  if (focusManager) {
    focusManager->releaseFocus(this);
  }
}

bool Widget::hasModalFocus() const {
  auto focusManager = getFocusManager();
  return focusManager && focusManager->hasModalFocus(this);
}

bool Widget::requestModalFocus() {
  if (!mFocusable) {
    return false;
  }
  
  auto focusManager = getFocusManager();
  return focusManager && focusManager->requestModalFocus(this);
}

void Widget::releaseModalFocus() {
  auto focusManager = getFocusManager();
  if (focusManager) {
    focusManager->releaseModalFocus(this);
  }
}

bool Widget::isParent (Widget *widget) const {
  return (widget == this) ||
         (mParent && mParent->isParent(widget));
}

Widget* Widget::findHotWidget(int x, int y) {
  for (auto it : boost::adaptors::reverse(mChildren)) {
    //Are the coordinates inside the widget ?
    if (x < it->GetX()) {
      continue;
    }
    if (y < it->GetY()) {
      continue;
    }
    if (x > it->GetX() + it->GetWidth()) {
      continue;
    }
    if (y > it->GetY() + it->GetHeight()) {
      continue;
    }
    
    // Point is inside the widget.
    auto res = it->findHotWidget(x - it->GetX(), y - it->GetY());
    if (res) {
      return res;
    } else if (it->IsMouseOpaque()) {
      // We can process further, we are blocked by this widget
      return nullptr;
    }
  }

  //No child widget matched... If we are focusable, return this.
  return mFocusable ? this : nullptr;
}

bool Widget::onKeyPressed(sf::Event::KeyEvent key) {
  return false;
}

bool Widget::onKeyReleased(sf::Event::KeyEvent key) {
  return false;
}

bool Widget::onTextEntered(sf::Uint32 key) {
  return false;
}

bool Widget::onMouseButtonPressed(int button, float x, float y) {
  return false;
}

bool Widget::onMouseButtonReleased(int button, float x, float y) {
  return false;
}

bool Widget::onMouseWheel(int delta, float x, float y) {
  return false;
}

bool Widget::onMouseOver() {
  return false;
}

bool Widget::onMouseOut() {
  return false;
}

bool Widget::onFocusGained() {
  return true;
}

bool Widget::onFocusLost() {
  return true;
}

bool Widget::dispatchEvent(const string& eventType) {
  if (mFocusable) {
    try {
      luabind::object callback = mLuaObject[eventType];
      if (luabind::call_function<bool>(callback, mLuaObject)) {
        return true;
      }
    } catch (luabind::error e) {
      cout << "Lua error : " << lua_tostring(e.state(), -1) << endl;
    }
  }
  
  if (mParent) {
    return mParent->dispatchEvent(eventType);
  }
  
  return false;
}

// XXX avoid code duplication here (event class? dynamic?)
bool Widget::dispatchEvent(const std::string& eventType, const sf::Event::KeyEvent &key) {
  if (mFocusable) {
    try {
      luabind::object callback = mLuaObject[eventType];
      if (luabind::call_function<bool>(callback, mLuaObject, key)) {
        return true;
      }
    } catch (luabind::error e) {
      cout << "Lua error : " << lua_tostring(e.state(), -1) << endl;
    }
  }
  
  if (mParent) {
    return mParent->dispatchEvent(eventType, key);
  }
  
  return false;
}

// XXX avoid code duplication here (event class? dynamic?)
bool Widget::dispatchEvent(const std::string& eventType, sf::Uint32 key) {
  if (mFocusable) {
    try {
      luabind::object callback = mLuaObject[eventType];
      if (luabind::call_function<bool>(callback, mLuaObject, key)) {
        return true;
      }
    } catch (luabind::error e) {
      cout << "Lua error : " << lua_tostring(e.state(), -1) << endl;
    }
  }
  
  if (mParent) {
    return mParent->dispatchEvent(eventType, key);
  }
  
  return false;
}

bool Widget::dispatchMouseEvent(const std::string& eventType, int i, float x, float y) {
  if (mFocusable) {
    try {
      luabind::object callback = mLuaObject[eventType];
      if (luabind::call_function<bool>(callback, mLuaObject, i, x, y)) {
        return true;
      }
    } catch (luabind::error e) {
      cout << "Lua error : " << lua_tostring(e.state(), -1) << endl;
    }
  }
  
  if (mParent) {
    return mParent->dispatchMouseEvent(eventType, i, x + GetX(), y + GetY());
  }
  
  return false;
}

void Widget::addChild(shared_ptr<Widget> child) {
  if (child->mParent) {
    // XXX log an error here
    return;
  }

  mChildren.push_back(child);
  child->mParent = this;
}

void Widget::detach() {
  if (!mParent) {
    // We are not attached, nothing to do!
    return;
  }

  removeFromGui();
  for (auto sharedPtr : mParent->mChildren) {
    if (sharedPtr.get() == this) {
      mParent->mDetachedChildren.push_back(sharedPtr);
      mParent->mChildren.remove(sharedPtr);
      break;
    }
  }
  mParent = nullptr;
}

list<shared_ptr<Widget> > Widget::getChildren() const {
  return mChildren;
}

shared_ptr<FocusManager> Widget::getFocusManager() const {
  if (mFocusManager) {
    return mFocusManager;
  }

  if (!mParent) {
    return nullptr;
  }

  mFocusManager = mParent->getFocusManager();
  return mFocusManager;
}

void Widget::RenderWidget (sf::RenderTarget& Target) const {
}

void Widget::RenderWidgetPostTreatment (sf::RenderTarget& Target) const {
}

void Widget::InternalUpdate (float time) {
  for (const auto child : mChildren) {
    child->InternalUpdate(time);
  }
  
  Update(time);
  mDetachedChildren.clear();
}

void Widget::Update (float time) {
}

void Widget::Render (sf::RenderTarget& Target) const {
  //Draws content
  RenderWidget(Target);
  
  //Draws all internal widgets
  for (const auto child : mChildren) {
    //Enable clipping
    Clipping::Push(ClipArea(child->GetX(), child->GetY(), child->GetWidth(), child->GetHeight()));
    
    //Draws widget
    Target.Draw(*child);
    
    //Disable clipping
    Clipping::Pop();
  }
  
  //Draws finitions
  RenderWidgetPostTreatment(Target);
}

void Widget::removeFromGui() {
  auto focusManager = getFocusManager();
  if (focusManager) {
    // First, we need to lose focus
    focusManager->removeWidget(this);

    // Then, our children need to remove themselves from the Gui too
    for (auto child : mChildren) {
      child->removeFromGui();
    }
  }
}

} // namespace
