/**
 * inGui
 *
 * Author : aurelien.fredouelle@gmail.com
 */

#pragma once

#include "Config.h"
#include <SFML/Graphics.hpp>
#include <list>

#include <lua.hpp>
#include <luabind/luabind.hpp>

#include <memory>

namespace inGui {

class FocusManager;
class Gui;

class INGUI_API Widget : public sf::Drawable {
 private:
  /**
   * This constructor is only used for the root. It is the only
   * widget that will be created from the C++ side.
   */
  Widget();

 public:
  /**
   * Constructor.
   *
   * This should never be called from C++ code (except when
   * constructing derived classes), the creation of new widgets
   * is the responsability of Lua.
   */
  Widget(Gui* gui);

  /**
   * Constructor used for the root.
   */
  static std::shared_ptr<Widget> createRoot();

  /**
   * Destructor
   */
  virtual ~Widget();

  static luabind::scope getLuaBindings();

  /**
   * Called from Lua to register on the C++ side a pointer
   * to the Lua object.
   * Exported to Lua.
   */
  void registerLuaObject(luabind::object obj);

  /**
   * Get the size of the widget
   *
   * @return Current size in a 2D vector
   */
  virtual const sf::Vector2f& GetSize() const;

  /**
   * Get the width of the widget.
   * Exported to Lua.
   *
   * @return Current width
   */
  virtual float GetWidth() const;

  /**
   * Get the height of the widget.
   * Exported to Lua.
   *
   * @return Current height
   */
  virtual float GetHeight() const;

  /**
   * Set the size of the widget (take 2 values).
   * Exported to Lua.
   *
   * @param Width : New width
   * @param Height : New height
   */
  virtual void SetSize(float Width, float Height);

  /**
   * Set the width of the widget.
   * Exported to Lua.
   *
   * @param Width : New width
   */
  virtual void SetWidth(float Width);

  /**
   * Set the height of the widget.
   * Exported to Lua.
   *
   * @param Height : New height
   */
  virtual void SetHeight(float Height);

  /**
   * Get the X position of the widget.
   * Exported to Lua.
   *
   * @return Current X coordinate
   */
  virtual float GetX() const;

  /**
   * Get the Y position of the widget.
   * Exported to Lua.
   *
   * @return Current Y coordinate
   */
  virtual float GetY() const;

  /**
   * Get the absolute X position of the widget.
   *
   * Exported to Lua.
   *
   * @return Current absolute X coordinate
   */
  virtual float GetAbsoluteX() const;

  /**
   * Get the absolute Y position of the widget.
   *
   * Exported to Lua.
   *
   * @return Current absolute Y coordinate
   */
  virtual float GetAbsoluteY() const;


  /**
   * Different possibilities for widgets' horizontal
   * alignment
   */
  enum HAlignment
  {
    HCENTER,
    LEFT,
    RIGHT
  };

  /**
   * Different possibilities for widgets' vertical
   * alignment
   */
  enum VAlignment
  {
    VCENTER,
    TOP,
    BOTTOM
  };

  /**
   * Set the widget's horizontal alignment.
   *
   * Exported to Lua.
   */
  virtual void SetHAlign(HAlignment a);

  /**
   * Set the widget's vertical alignment.
   *
   * Exported to Lua.
   */
  virtual void SetVAlign(VAlignment a);

  /**
   * Get the font used by the widget
   */
  virtual std::shared_ptr<sf::Font> GetFont() const;

  /**
   * Set the font used by the widget
   */
  virtual void SetFont(std::shared_ptr<sf::Font> font);

  /**
   * Set the font used by the widget.
   * Exported to Lua.
   */
  virtual void SetFont(const std::string &name, unsigned int size);

  /**
   * Get the focusable status of the widget.
   * Exported to Lua.
   */
  virtual bool isFocusable() const;

  /**
   * Set the focusable status of the widget.
   * Exported to Lua.
   */
  virtual void setFocusable(bool e = true);


  /**
   * Get the mouse opacity status of the widget.
   * Exported to Lua.
   */
  virtual bool IsMouseOpaque() const;

  /**
   * Set the mouse opacity status of the widget.
   * Exported to Lua.
   */
  virtual void SetMouseOpaque(bool e = true);


  /**
   * Indicates if the widget has currently the focus.
   * Exported to Lua.
   */
  virtual bool hasFocus() const;

  /**
   * Requests focus. Returns true in case of success,
   * returns false if the widget is not focusable or
   * if the request couldn't be granted (another widget
   * has modal focus).
   * Exported to Lua.
   */
  virtual bool requestFocus();

  /**
   * Releases focus (and modal focus if owned by this widget).
   * Exported to Lua.
   */
  virtual void releaseFocus();

  /**
   * Indicates if the widget has currently the modal focus.
   * Exported to Lua.
   */
  virtual bool hasModalFocus() const;

  /**
   * Requests modal focus. Returns true in case of success,
   * returns false if the widget is not focusable or
   * if the request couldn't be granted (another widget
   * has modal focus).
   * Exported to Lua.
   */
  virtual bool requestModalFocus();

  /**
   * Releases modal focus, but keeps the "standard" focus.
   * If the widget hadn't the focus, no effect.
   * Exported to Lua.
   */
  virtual void releaseModalFocus();

  /**
   * Detach from the parent container (Widget or Gui).
   *
   * If the widget is not attached, fails silently.
   */
  virtual void detach();

  /**
   * Indicates if the widget passed in parameter is a
   * parent of this widget.
   */
  virtual bool isParent(Widget *widget) const;


  /**
   * Find the top-most focusable child widget under coordinates (x,y).
   */
  virtual Widget* findHotWidget(int x, int y);

  /**
   * Callback used when a key is pressed when this widget
   * has the focus.
   *
   * Returns a boolean : true if the event has been consumed,
   * false if it should be forwarded deeper in widgets hierarchy.
   * Exported to Lua.
   */
  virtual bool onKeyPressed(sf::Event::KeyEvent key);

  /**
   * Callback used when a key is released when this widget
   * has the focus.
   *
   * Returns a boolean : true if the event has been consumed,
   * false if it should be forwarded deeper in widgets hierarchy.
   * Exported to Lua.
   */
  virtual bool onKeyReleased(sf::Event::KeyEvent key);

  /**
   * Callback used when a character is typed when this widget
   * has the focus.
   *
   * Returns a boolean : true if the event has been consumed,
   * false if it should be forwarded deeper in widgets hierarchy.
   *
   * Exported to Lua.
   */
  virtual bool onTextEntered(sf::Uint32 key);

  /**
   * Callback used when a mouse button is clicked while this widget
   * is hot.
   *
   * @param button Identifier of the pressed button
   * @param x Horizontal position of the event, relative to the widget
   * @param y Vertical position of the event, relative to the widget
   *
   * Returns a boolean : true if the event has been consumed,
   * false if it should be forwarded deeper in widgets hierarchy.
   *
   * Exported to Lua.
   */
  virtual bool onMouseButtonPressed(int button, float x, float y);

  /**
   * Callback used when a mouse button is released while this widget
   * is hot.
   *
   * @param button Identifier of the released button
   * @param x Horizontal position of the event, relative to the widget
   * @param y Vertical position of the event, relative to the widget
   *
   * Returns a boolean : true if the event has been consumed,
   * false if it should be forwarded deeper in widgets hierarchy.
   *
   * Exported to Lua.
   */
  virtual bool onMouseButtonReleased(int button, float x, float y);

  /**
   * Callback used when for mouse wheel events.
   *
   * @param delta Describes how much the mouse wheel turned, can be negative
   * @param x Horizontal position of the event, relative to the widget
   * @param y Vertical position of the event, relative to the widget
   *
   * Returns a boolean : true if the event has been consumed,
   * false if it should be forwarded deeper in widgets hierarchy.
   *
   * Exported to Lua.
   */
  virtual bool onMouseWheel(int delta, float x, float y);

  /**
   * Callback used when this widget becomes hot (ie gets the mouse).
   *
   * Returns a boolean : true if the event has been consumed,
   * false if it should be forwarded deeper in widgets hierarchy.
   * Exported to Lua.
   */
  virtual bool onMouseOver();

  /**
   * Callback used when this widget loses the hot status
   * (ie looses the mouse).
   *
   * Returns a boolean : true if the event has been consumed,
   * false if it should be forwarded deeper in widgets hierarchy.
   * Exported to Lua.
   */
  virtual bool onMouseOut();

  /**
   * Callback used when the widget gains the focus.
   *
   * Returns a boolean : true if the event has been consumed,
   * false if it should be forwarded deeper in widgets hierarchy.
   * Exported to Lua.
   */
  virtual bool onFocusGained();

  /**
   * Callback used when the widget loses the focus.
   *
   * Returns a boolean : true if the event has been consumed,
   * false if it should be forwarded deeper in widgets hierarchy.
   * Exported to Lua.
   */
  virtual bool onFocusLost();

  virtual std::list<std::shared_ptr<Widget> > getChildren() const;

  /**
   * Distributes an event to the widget, and then
   * to its parents, until the event is consumed.
   *
   * @return Boolean telling if the event has been consumed
   */
  bool dispatchEvent(const std::string& eventType);
  bool dispatchEvent(const std::string& eventType, const sf::Event::KeyEvent &key);
  bool dispatchEvent(const std::string& eventType, sf::Uint32 key);
  bool dispatchMouseEvent(const std::string& eventType, int i, float x, float y);


  /**
   * Calls all child widget's Update() method, and then
   * this widget's one.
   *
   * @param time The time elapsed since the last update
   */
  void InternalUpdate(float time);

  /**
   * Adds a child widget.
   *
   * @param Widget Pointer to the widget to add
   */
  virtual void addChild(std::shared_ptr<Widget> widget);

  std::shared_ptr<FocusManager> getFocusManager() const;
 protected:

  /**
   * Draws the widget content
   */
  virtual void RenderWidget(sf::RenderTarget& Target) const;

  /**
   * Add more to the widget's drawing, after its children are drawn.
   */
  virtual void RenderWidgetPostTreatment(sf::RenderTarget& Target) const;


  /**
   * Updates the widget. This function gets called each frame.
   *
   * @param time The time elapsed since the last update
   */
  virtual void Update(float time);

  /**
   * Parent widget
   */
  Widget* mParent;

  /**
   * Reference to the Lua version of the object.
   * Used to call Lua methods.
   */
  luabind::object mLuaObject;

  /**
   * List of the child widgets
   */
  std::list<std::shared_ptr<Widget> > mChildren;

  /**
   * List of widgets that were just detached from this one.
   *
   * The point of this list is to keep a reference to those widgets
   * until the end of the tick, this way if this is the last reference
   * they won't be delete while calling detach().
   */
  std::list<std::shared_ptr<Widget> > mDetachedChildren;

  /**
   * Size of the widget
   */
  sf::Vector2f mSize;

  /**
   * Boolean telling if the widget can get focus.
   */
  bool mFocusable;

  /**
   * Boolean telling if the "mouse hotness" can go
   * through this widget (only useful if not focusable)
   */
  bool mMouseOpaque;

  /**
   * Font used by this widget and its children
   */
  std::shared_ptr<sf::Font> mFont;

  /**
   * Caches the focus manager for this widget.
   *
   * At first this pointer is invalid. When getFocusManager() is called,
   * this pointer is updated by checking up the widgets hierarchy.
   * Either this widget is connected to the root, in which case the focus
   * manager will be set to the one of the root, or the widget is unconnected,
   * and mFocusManager will remain invalid.
   */
  mutable std::shared_ptr<FocusManager> mFocusManager;

 private:
  /**
   * Make sure this widget and its children are not referenced anywhere
   * in the GUI anymore (release focus, etc...).
   */
  void removeFromGui();

  //Inherited from sf::Drawable
  void Render(sf::RenderTarget& Target) const;
};

} // namespace
