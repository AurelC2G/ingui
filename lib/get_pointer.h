/**
 * inGui
 *
 * Author : aurelien.fredouelle@gmail.com
 */

#pragma once

#include <memory>

namespace inGui {

template<class T> inline T * get_pointer(std::shared_ptr<T> const & p)
{
    return p.get();
}

} // namespace
