/**
 * inGui
 *
 * Author : aurelien.fredouelle@gmail.com
 *
 * inGui.hpp
 */

//Dependencies
#include <SFML/Graphics.hpp>

//Core classes
#include "Gui.h"
#include "Widget.h"
