
-- Add shortcuts for the color functions
local widgetColorTypes = {
  Frame = {"background", "border"};
  Label = {"text"};
  Button = {"text", "background"};
  ImageButton = {"background"};
  Checkbox = {"tick", "background", "border"};
  Icon = {"background"};
  TextField = {"text", "background"};
}

local function genColorSetter(colorType)
  return function (this, colorOrRed, g, b, alpha)
    if type(colorOrRed) == 'userdata' then
      this:__setColor(colorType, colorOrRed)
   else
      local r = colorOrRed
      if alpha == nil then
        alpha = 255
      end
      this:__setColor(colorType, inGui.Color(r, g, b, alpha))
    end
  end
end

for widgetName,colorTypes in pairs(widgetColorTypes) do
  local w = inGui.__widgets[widgetName]
  
  for t in values(colorTypes) do
    w["get" .. ucfirst(t) .. "Color"] = function (this)
      return this:__getColor(t)
    end

    w["set" .. ucfirst(t) .. "Color"] = genColorSetter(t)
  end

  if #colorTypes == 1 then
    local t = colorTypes[1]
    function w.getColor(this)
      return this:__getColor(t)
    end
    w.setColor = genColorSetter(t)
  end
end
