local description = [=[
Usage: lua bin2c.lua filename outname [--compile]

Generate a C source file to embed the provided Lua code.

The file named 'filename' contains either Lua byte code or Lua source.
If the --compile option is provided, then the contents of 'filename' are first
compiled before being used to generate the C output.
]=]

if not arg or #arg < 2 or #arg > 3 then
  io.stderr:write(description)
  return
end

local compile = false
if #arg == 3 then
  if arg[3] == "--compile" then
    compile = true
  else
    io.stderr:write(description)
    return
  end
end

local filename = arg[1]
local varName = arg[2]

local content
if compile then
  content = string.dump(assert(loadfile(filename)))
else
  content = assert(io.open(filename,"rb")):read"*a"
end

local numtab = {}
for j=0,255 do
  numtab[string.char(j)]=("%3d,"):format(j)
end
content = content:gsub(".", numtab):gsub(("."):rep(80), "%0\n")

io.write(string.format([=[
#include <stdlib.h>
const char %s[] = {
%s
};
const size_t %s_length = sizeof(%s);
]=], varName, content, varName, varName))

