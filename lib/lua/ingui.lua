
function inGui.createWidget(type, attributes)
  if inGui.__widgets[type] == nil then
    error("Invalid widget type: " .. type)
  end
  local obj = inGui.__widgets[type](inGui.Gui)
  obj:registerLuaObject(obj)
  if attributes ~= nil then
    obj:setAttributes(attributes)
  end
  return obj
end

-- Extend the widget types
for widgetName,widgetClass in pairs(inGui.__widgets) do
  widgetClass.name = widgetName
end
do
  local methods = {}
  function methods.attachToRoot(widget)
    inGui.Gui:attachWidget(widget)
  end

  function methods.setAttributes(widget, attributes)
    for aName, aValue in pairs(attributes) do
      local methodName = "set" .. ucfirst(aName)
      local f = widget[methodName]
      if type(f) ~= "function" then
        error(string.format("Unknown method %s::%s", widget.name, methodName))
      elseif type(aValue) == "table" then
        f(widget, unpack(aValue))
      else
        f(widget, aValue)
      end
    end
  end

  function methods.addNew(widget, type, attributes)
    local obj = inGui.createWidget(type, attributes)
    widget:add(obj)
    return obj
  end

  -- Attach those methods to all the widget types
  for w in values(inGui.__widgets) do
    for methodName,method in pairs(methods) do
      w[methodName] = method
    end
  end
end
