
-- Iterator which returns all the keys in a table
function keys(t)
  local k
  local function it(t)
    k = next(t, k)
    return k
  end
  return it, t, 0
end

-- Iterator which returns all the values in a table
function values(t)
  local k
  local function it(t)
    local v
    k,v = next(t, k)
    return v
  end
  return it, t, 0
end

-- Capitalize the first letter of a string
function ucfirst(s)
  if not #s then
    return ""
  end
  return s:sub(1,1):upper() .. s:sub(2)
end
